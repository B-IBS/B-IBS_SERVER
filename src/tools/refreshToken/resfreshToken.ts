import * as jwt from 'jsonwebtoken';
import { prisma } from '../../generated/prisma-client';
import access from '../../config/access';
import { jwtKey } from '../../config/secret_key';

interface UserId {
  id: string;
}

const expireIn = 30 * 24 * 60 * 60 * 1000;

const getRefreshToken = async (user: UserId): Promise<string> => {
  const exist = await prisma.refreshTokens({ where: { user: { id: user.id } } });
  let token: string;

  if (exist.length === 0) {
    token = jwt.sign({ id: user.id, authorisation: access.OWNER }, jwtKey, {
      algorithm: 'HS256',
      expiresIn: expireIn,
    });
    await prisma.createRefreshToken({ user: { connect: { id: user.id } }, refreshToken: token });
  } else {
    token = exist[0].refreshToken;
  }
  return token;
};

const generateRefreshToken = async (user: UserId): Promise<string> => {
  await prisma.deleteManyRefreshTokens({ user: { id: user.id } });
  const token = jwt.sign({ id: user.id, authorisation: access.OWNER }, jwtKey, {
    algorithm: 'HS256',
    expiresIn: expireIn,
  });
  await prisma.createRefreshToken({
    user: { connect: { id: user.id } },
    refreshToken: token,
  });
  return token;
};

export {
  generateRefreshToken,
  getRefreshToken,
};
