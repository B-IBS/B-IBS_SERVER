import notificationAdmin from './index';

class NotificationWrapper {
  sendNotification = async (registrationToken: string, message: any):
  Promise<void> => {
    const messageData = { ...message, token: registrationToken };
    notificationAdmin.messaging().send(messageData).then((response) => {
      console.warn('Successfully sent message:', response);
    }).catch((e) => {
      console.error('Error sending message:', e);
    });
  };

  sendNotifications = async (registrationTokens: [string], message: any): Promise<void> => {
    const messageData = { ...message, tokens: registrationTokens };
    notificationAdmin.messaging().sendMulticast(messageData).then((response) => {
      if (response.failureCount > 0) {
        const failedTokens = [];
        response.responses.forEach((resp, idx) => {
          if (!resp.success) {
            failedTokens.push(registrationTokens[idx]);
          }
        });
        console.warn(`List of tokens that caused failures: ${failedTokens}`);
      }
    });
  };
}

export default NotificationWrapper;
