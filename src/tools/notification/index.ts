import admin = require('firebase-admin');

const notificationAdmin = admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://bibs.bibs.firebaseio.com/',
});

export default notificationAdmin;
