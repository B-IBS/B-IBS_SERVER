import { prisma } from './generated/prisma-client';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import notificationAdmin from './tools/notification';
import jobs from './jobs';
import formatErrorDebug from './error/formatError';

import server from './config/server';

const PORT = (process.env.PORT ? process.env.PORT : 4000);

const options = {
  port: PORT,
  formatError: formatErrorDebug,
};

server.start(options, async ({ port }) => {
  // eslint-disable-next-line no-console
  console.log(`Server is running on http://localhost:${port}`);
  jobs();
});

prisma.deleteManyTokenConnections({ expiration_date_lte: new Date().toString() });
