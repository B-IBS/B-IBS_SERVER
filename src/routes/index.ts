import debug from './debug';
import doctorPdf from './doctorPdf';

export default (app): void => {
  debug(app);
  doctorPdf(app);
};
