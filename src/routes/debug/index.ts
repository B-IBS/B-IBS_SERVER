// eslint-disable-next-line @typescript-eslint/no-unused-vars
const helloworld = (req, res, _): void => {
  res.send({
    status: true,
    message: 'Hello world !',
  });
};

export default (app): void => {
  app.get('/helloworld', helloworld);
  app.post('/helloworld', helloworld);
};
