import * as React from 'react';
import { Calendar } from "@nivo/calendar";
import { intensityColors, months } from "./utils";

const NivoCalendar = ({ data }): JSX.Element => {
  return (
    <Calendar
      data={data}
      height={200}
      width={1000}
      from={data[0].day}
      to={data[data.length - 1].day}
      monthLegend={(_, month, __): string => months[month]}
      emptyColor="#eeeeee"
      colors={intensityColors}
      minValue={0}
      maxValue={5}
      margin={{ top: 0, right: 0, bottom: 0, left: 20 }}
      yearSpacing={40}
      monthBorderColor="#ffffff"
      dayBorderWidth={2}
      dayBorderColor="#ffffff"
      monthSpacing={15}
      legends={[
          {
              anchor: 'bottom-right',
              direction: 'row',
              translateY: 36,
              itemCount: 4,
              itemWidth: 42,
              itemHeight: 36,
              itemsSpacing: 14,
              itemDirection: 'right-to-left'
          }
      ]}
    />
  );
};

export default NivoCalendar;