import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import * as puppeteer from 'puppeteer';
import { Response, Request } from 'express';

import { Food, Meal, prisma, TokenConnection, User } from '../../generated/prisma-client';

import { CrisesData } from './utils';
import hexRgb from './hexRgb';
import Page from './Page';

const renderTemplate = (crises: Array<CrisesData>, meals: Array<Meal & { food: Array<Food> }>, user: User): string => {
  const markup = ReactDOMServer.renderToStaticMarkup(
    React.createElement(Page, { crises, meals, user })
  );

  return `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0">
        ${markup}
      </body>
    </html>
  `;
}

const renderPDF = async (html: string): Promise<Buffer> => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  })
  const page = await browser.newPage();
  await page.setContent(html, { waitUntil: 'networkidle0' });
  const pdf = await page.pdf({ format: 'a4', printBackground: true });
  await browser.close();
  return pdf;
};

const hexToRgb = (str: string): string => {
  const hexTest = /#[a-f\d]{3,6}/gim
  return str.replace(hexTest, hexColor => {
    const { red, green, blue } = hexRgb(hexColor)
    return `rgb(${red}, ${green}, ${blue})`
  })
}

const getUserOrCode = async (user: User | null, code: string | undefined): Promise<User | null> => {
  if (user)
    return user;
  if (code) {
    const { tokenConnections }: {tokenConnections: Array<TokenConnection & { user: User }>} = await prisma.$graphql(`query { tokenConnections(where: {token: "${code}"}) {user {id} expiration_date}}`);
    if (tokenConnections.length) {
      const foundUser = await prisma.user({ id: tokenConnections[0].user.id });
      return foundUser;
    } else {
      return null;
    }
  }
  return null;
};

interface RequestAuth extends Request {
  user: User;
}
const generateDoctorPdf = async (req: RequestAuth, res: Response, _): Promise<void> => {
  const user = await getUserOrCode(req.user, req.query.code as string);

  if (!user) {
    res.status(500).send('No user');
    return;
  }

  const crises = await prisma.crises({
    where: { user: { id: user.id } },
  });
  const { meals }: {meals: Array<Meal & { food: Array<Food> }>} = await prisma.$graphql(`
    query {
      meals(
        where: {
          user: { id: "${user.id}" }
        }
        orderBy: date_DESC
        first: 10
      ) {
        id
        createdAt
        updatedAt
        name
        date
        notes
        food {
          id
          name
          caution
          level
        }
      }
    }`
  );
  const userMeals = await Promise.all(meals.map(async (meal) => {
    const userFoods = await Promise.all(meal.food.map(async (food) => {
      const userFood = await prisma.userFoods({ where: { user: { id: user.id }, food: { id: food.id }}});
      const level = userFood.length ? userFood[0].level : food.level;
      return { ...food, level };
    }));
    return { ...meal, food: userFoods };
  }))

  const crisesData = crises
    .sort((a, b) => (new Date(a.date)).getTime() - (new Date(b.date)).getTime())
    .map((c) => {
      const date = new Date(c.date);
      const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;
      return {
        day: `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`,
        value: Number(c.intensity),
        date: date,
      }
    })
  const html = renderTemplate(crisesData, userMeals, user);
  const pdf = await renderPDF(hexToRgb(html));
  res.contentType('application/pdf');
  res.setHeader('Content-Disposition', `recapitulatifPatient-${user.name}-${(new Date()).toLocaleDateString('fr-FR')}.pdf`);
  res.send(pdf);
};

export default (app): void => {
  app.get('/generate-doctor-pdf', generateDoctorPdf);
};
