export interface CrisesData {
  value: number;
  day: string;
  date: Date;
}

export const intensityColors = [
  '#61cdbb',
  // '#73d4c4',
  '#85dbcc',
  // '#97e3d5',
  '#b2d8c3',
  // '#cdccb2',
  '#e7c1a0',
  '#eca88b',
  // '#f08f76',
  '#f47560'
];
export const months = [
  'Jan',
  'Fev',
  'Mar',
  'Avr',
  'Mai',
  'Jun',
  'Jul',
  'Aou',
  'Sept',
  'Oct',
  'Nov',
  'Dec'
];
export const days = [
  "Lundi",
  "Mardi",
  "Mercredi",
  "Jeudi",
  "Vendredi",
  "Samedi",
  "Dimanche"
];