import * as React from 'react';
import { Food, Meal, User } from '../../generated/prisma-client';

import { CrisesData, intensityColors } from './utils';

import CalendarGraph from './CalendarGraph';
import LineGraph from './LineGraph';

interface TemplateProps {
  crises: Array<CrisesData>;
  meals: Array<Meal & { food: Array<Food> }>;
  user: User;
}
const Template = ({ crises, meals, user }: TemplateProps): JSX.Element => {
  const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

  const today = new Date();
  const lastMonthData = crises.filter((d) => (new Date(d.day)).getMonth() === today.getMonth());

  return (
    <div style={{ fontFamily: "Roboto" }}>
      <div>
        <h1 style={{margin: "3rem 1rem"}}>
          Récapitulatif Patient: {user.name} {user.firstname}
        </h1>
        <hr/>
        <div style={{margin: "1rem"}}>
          <div style={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
            <h4>Intensité des crises survenues :</h4>
            <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
              0&nbsp;&nbsp;
              {intensityColors.map((e, i) => (
                <div key={i} style={{ width: 16, height: 16, backgroundColor: e}}/>
              ))}
              &nbsp;&nbsp;5
            </div>
          </div>
          <CalendarGraph data={crises} />
        </div>
        <hr/>
        <div style={{margin: "1rem"}}>
          <h4>Intensité moyenne des crises sur les jours de la semaine :</h4>
          <div style={{display: "flex", alignItems: "center", justifyContent: "center" }}>
            <LineGraph data={crises} />
          </div>
        </div>
        <hr/>
        <div style={{margin: "1rem"}}>
          <h4>Repas récents:</h4>
          <table style={{ borderSpacing: 0, fontSize: "0.75rem" }}>
            <thead>
              <th style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                Date
              </th>
              <th style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                Plat
              </th>
              <th style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                Ingrédients
              </th>
              <th style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                Risque
              </th>
            </thead>
            {meals.map((meal, i) => {
              const date = new Date(meal.date);
              const levelNb = meal.food.reduce((acc, food) => acc + (food.level > 0 ? 1 : 0), 0);
              const foodNb = meal.food.length;

              return (
                <tr key={i}>
                  <td style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                    {date.toLocaleDateString('fr-FR')}
                    &nbsp;à&nbsp;
                    {date.toLocaleTimeString('fr-FR')}
                  </td>
                  <td style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                    {meal.name}
                  </td>
                  <td style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                    {meal.food.map((food, u) => (
                      <React.Fragment key={u}>
                        {food.level > 0 ? (
                          <b>{food.name}</b>
                        ) : food.name }
                        {u < meal.food.length - 1 && " - "}
                      </React.Fragment>
                    ))}
                  </td>
                  <td style={{ border: "1px solid #eeeeee", padding: "0.5rem", margin: 0}}>
                    {levelNb === 0 ? "Très bas" : levelNb < foodNb * 0.4 ? "Bas" : levelNb < foodNb * 0.75 ? "Moyen" : "Haut"}
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
        <hr/>
        <div style={{margin: "1rem"}}>
          <h4>Données:</h4>
          <p>
            <i>Jours avec crises d&apos;intensité supérieure ou égales à 3:</i>&nbsp;
            <b>{crises.filter((d) => d.value >= 3).length}</b>
            <br/>
            <i>Dont depuis le dernier mois:</i>&nbsp;
            <b>{lastMonthData.filter((d) => d.value >= 3).length}</b>
            <br/>
          </p>
          <p>
            <i>Moyenne d'intensité des crises:</i>&nbsp;
            <b>{Math.round(crises.reduce((acc, d) => acc + d.value, 0) / crises.length)}</b>
            <br/>
            <i>Dont depuis le dernier mois:</i>&nbsp;
            <b>{Math.round(lastMonthData.reduce((acc, d) => acc + d.value, 0) / lastMonthData.length) || 0}</b>
            <br/>
          </p>
        </div>
      </div>
      <div
        style={{
          position: "absolute",
          left: 10,
          bottom: 0,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <img
          src="data:image/svg+xml;base64,PHN2ZyBpZD0iQ2FscXVlXzEiIGRhdGEtbmFtZT0iQ2FscXVlIDEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDQyNS4yIDQyNS4yIj48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6IzE0MTQxMjt9LmNscy0ye2ZpbGw6I2Y3ZjdmNzt9LmNscy0ze2ZpbGw6Izg4YmU4Yzt9PC9zdHlsZT48L2RlZnM+PHRpdGxlPkxPR09fQklCU18xNTB4MTUwbW08L3RpdGxlPjxjaXJjbGUgY2xhc3M9ImNscy0xIiBjeD0iMjEyLjYiIGN5PSIyMTIuNiIgcj0iMTI1LjMiLz48cGF0aCBjbGFzcz0iY2xzLTIiIGQ9Ik0yMzYuNDgsMjYyLjI5QTMxLjI3LDMxLjI3LDAsMCwxLDI2MCwyMzJjLS44LTE1LjcxLTkuNzctMjMuODMtMjQtMjkuMjYsOS4xNy01LjE0LDE2Ljg4LTEzLjIsMTYuODgtMjcuN3YtLjM3YzAtOC44LTIuOTMtMTYtOC44LTIxLjgzLTcuMzQtNy4zMy0xOC45LTExLjM3LTMzLjU3LTExLjM3SDE1MC45NFYyNjkuODlIMjEyYTc0LjY0LDc0LjY0LDAsMCwwLDI0LjcxLTMuNzJBMzEsMzEsMCwwLDEsMjM2LjQ4LDI2Mi4yOVptLTU4LTk2LjA1aDI3Ljg4YzExLjkyLDAsMTguNTMsNC43OCwxOC41MywxMy4yMnYuMzZjMCw5LjU0LTcuODksMTMuNTctMjAuMzYsMTMuNTdoLTI2Wk0yMzIsMjMxLjE4YzAsOS41NS03LjUyLDEzLjk1LTIwLDEzLjk1SDE3OC40NlYyMTYuODdoMzIuNjVjMTQuNDksMCwyMC45MSw1LjMzLDIwLjkxLDEzLjk1WiIvPjxjaXJjbGUgY2xhc3M9ImNscy0zIiBjeD0iMjY3Ljc3IiBjeT0iMjYyLjI5IiByPSIyMi40NiIvPjwvc3ZnPg=="
          width="60"
          height="60"
        />
        <h4 style={{ color: "#88bf8b"}}>Reprenez le contrôle</h4>
      </div>
      <p style={{ position: "absolute", right: 10, bottom: 0, color: "#444" }}>
        B.IBS - {user.name} {user.firstname} - {`${pad(today.getDate())}/${pad(today.getMonth() + 1)}/${today.getFullYear()}`}
      </p>
    </div>
  );
};

export default Template;