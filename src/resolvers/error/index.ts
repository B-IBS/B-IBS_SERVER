import queriesError from './queriesError';
import mutationsError from './mutationsError';

export default {
  Query: {
    ...queriesError,
  },
  Mutation: {
    ...mutationsError
  }
};
