import { ErrorLog, prisma } from "../../generated/prisma-client";
import { adminMiddleware } from '../../middleware/authentification';
import MyError from "../../error/MyError";

/*
 * query: getAllErrors(): ErrorLog[]!
 * @return: ErrorLog[]! : All available errors.
 *
 * Return a list of all errors in the database
 */
const getAllErrors = async (): Promise<ErrorLog[]> => {
  const errors = await prisma.errorLogs();

  if (!errors) throw new MyError(500, "Couldnt find errors in db", "NoErrors")
  return errors
};

export default {
  getAllErrors: adminMiddleware(getAllErrors),
};
