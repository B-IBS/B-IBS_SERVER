import { prisma } from "../../generated/prisma-client";
import { InvalidArgumentError } from "../../error/AuthError";
import { adminMiddleware } from "../../middleware/authentification";

interface DeleteErrorArgs {
  id: string;
}

/*
 * mutation: deleteError(errorId: ID!): Boolean!
 * @param errorRd ID! : Error to delete Id.
 * @return: Boolean : True if the Error is correctly deleted.
 *
 * Edit Food data.
 */
const deleteError = async (_: any, arg: DeleteErrorArgs, __: any): Promise<boolean> => {
  const exist = await prisma.errorLogs({ where: { id: arg.id.toString() } });

  if (!exist) throw new InvalidArgumentError(409, `ErrorLog ${arg.id} doesnt exist`);
  await prisma.deleteErrorLog({
    id: arg.id.toString()
  })
  return true;
};

export default {
  deleteError: adminMiddleware(deleteError)
}
