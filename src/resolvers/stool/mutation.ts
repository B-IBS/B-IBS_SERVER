import {AuthError} from '../../error/AuthError';
import {prisma} from '../../generated/prisma-client';

interface AddStoolDataArg {
  bristolScale: number;
  isCrisisInduced: boolean;
  date: string;
}

const addStoolData = async (parent: any, arg: AddStoolDataArg, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  await prisma.createStoolData({
    date: arg.date,
    bristolScale: arg.bristolScale,
    isCrisisInduced: arg.isCrisisInduced,
    user: {connect: {id: context.request.user.id}},
  });

  return true;
}

export default {
  addStoolData
}
