import getStool from './query';
import AddStoolData from './mutation';

export default {
  Query: { ...getStool },
  Mutation: { ...AddStoolData },
};
