import { prisma, StoolData } from "../../generated/prisma-client";
import { AuthError } from "../../error/AuthError";

const getStool = async (parent: any, _: null, context: any): Promise<Array<StoolData>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  const stools = await prisma.stoolDatas({
    where: {
      user: {
        id: context.request.user.id
      }
    },
  });

  return stools
}

export default {
  getStool
}
