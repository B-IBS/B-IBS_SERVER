import addUserMedicines from './addUserMedicines';
import getMedicine from './getMedicine';
import addMedicine from './addMedicine';
import getUserMedicine from './getUserMedicine';
import searchMedicines from "./searchMedicines";

export default {
  Query: {
    ...getMedicine,
    ...getUserMedicine,
    ...searchMedicines,
  },
  Mutation: {
    ...addUserMedicines,
    ...addMedicine,
  },
};
