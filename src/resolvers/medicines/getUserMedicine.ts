import {prisma, UserMedicine} from '../../generated/prisma-client';

/*
 * query: query/mutation prototype
 * @param: foo Type : argument name and it's type.
 * @return: Type : return type and a small description.
 *
 * Explain what this query/mutation does.
 */
const getUserMedicine = async (parent: any, _: any, context: any): Promise<UserMedicine[]> => {
  const user = await prisma.userMedicines({
    where: {
      user: { id: context.request.user.id },
    },
  });
  return user;
};

export default {
  getUserMedicine,
};
