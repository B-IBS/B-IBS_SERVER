import { prisma } from '../../generated/prisma-client';
import * as  medicine from './medicaments.json'; // This import style requires "esModuleInterop", see "side notes"
import { AuthError } from '../../error/AuthError';

interface AddMedicineArg {
  name: string;
  imageUrl: string;
  details: string;
}
/*
 * mutation: addMedicine(name: String details: String imageUrl: String): Boolean!
 * @param: name : Medicine name.
 * @param: details : Medicine details.
 * @param: imageUrl : URL of a drug photo .
 * @return: Type : Type : True if the medicine is correctly added.
 *
 * Add a new food to the medicine data base.
 */

const addMedicine = async (parent: any, arg: AddMedicineArg, context: any): Promise<boolean> => {

  if (!context.request.user.isAdmin) throw new AuthError(401, 'This query needs an admin user');
  if (arg.name == null) {
    for (const val of medicine) {
      const split = val["title"].split(",", 2);

      await prisma.createMedicine({
        name: split[0],
        details: split[1],
      });
    }
  } else {
    await prisma.createMedicine({
      name: arg.name,
      details: arg.details,
      imageUrl: arg.imageUrl,
    });
  }
  return true;
};

export default {
  addMedicine,
};
