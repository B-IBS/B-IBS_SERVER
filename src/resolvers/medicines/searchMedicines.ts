import {Medicine, prisma} from '../../generated/prisma-client';

interface GetMedicineArg {
    name: string;
}

/*
 * query: searchMedicine(name: String!): [Medicine!]!
 * @param name String! : Medicine name or partial name.
 * @return: [Medicine!]! : List of Medicine that could correspond to what was asked.
 *
 * Return a list of Medicine that can correspond to the name pass or the partial name passed as argument.
 */

const searchMedicine = async (parent: any, arg: GetMedicineArg, _: any): Promise<Medicine[]> => {

    const med = await prisma.medicines({
        where: {
            name_starts_with: arg.name,
        },
    })

    return med;
};

export default {
    searchMedicine,
};
