import { prisma } from '../../generated/prisma-client';
import { NoneMetricsAuthError } from '../../error/AuthError';

interface AddUserMedicinesArg {
  name: string;
}
/*
 * query: query/mutation prototype
 * @param: foo Type : argument name and it's type.
 * @return: Type : return type and a small description.
 *
 * Explain what this query/mutation does.
 */

const addUserMedicines = async (parent: any, arg: AddUserMedicinesArg, context: any): Promise<boolean> => {
  const medicine = await prisma.medicine({ name: arg.name });
  if (!context.request.user) throw new NoneMetricsAuthError(401, 'This query need a connected user');
  if (!medicine) throw new NoneMetricsAuthError(400, `${arg.name} n'existe pas dans la base`);
  await prisma.createUserMedicine({ user: { connect: { id: context.request.user.id } }, medicines: { connect: { id: medicine.id } } });
  const profil = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  await prisma.updateProfil({ data: { nb_medicine: profil[0].nb_medicine + 1 }, where: { id: profil[0].id } });
  return true;
};

export default {
  addUserMedicines,
};
