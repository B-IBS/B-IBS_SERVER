import {Medicine, prisma} from '../../generated/prisma-client';

interface GetMedicineArg {
  name: string;
}
/*
 * query: query/mutation prototype
 * @param: foo Type : argument name and it's type.
 * @return: Type : return type and a small description.
 *
 * Explain what this query/mutation does.
 */
const getMedicine = async (parent: any, arg: GetMedicineArg, _: any): Promise<Medicine> => {
  const med = await prisma.medicine({ name: arg.name });
  return med;
};

export default {
  getMedicine,
};
