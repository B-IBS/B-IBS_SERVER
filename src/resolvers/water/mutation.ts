import {AuthError} from '../../error/AuthError';
import {prisma} from '../../generated/prisma-client';

interface AddWaterDataArg {
  date: string;
  quantity: number;
}

const addWaterData = async (parent: any, arg: AddWaterDataArg, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  await prisma.createWaterData({
    date: arg.date,
    quantity: arg.quantity,
    user: {connect: {id: context.request.user.id}},
  });

  return true;
}

export default {
  addWaterData
}
