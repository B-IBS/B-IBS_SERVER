import getWater from './query';
import AddWaterData from './mutation';

export default {
  Query: { ...getWater },
  Mutation: { ...AddWaterData },
};
