import { prisma, WaterData } from "../../generated/prisma-client";
import { AuthError } from "../../error/AuthError";

const getWater = async (parent: any, _: null, context: any): Promise<Array<WaterData>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  const water = await prisma.waterDatas({
    where: {
      user: {
        id: context.request.user.id
      }
    },
  });

  return water
}

export default {
  getWater
}
