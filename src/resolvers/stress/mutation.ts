import {AuthError} from '../../error/AuthError';
import {prisma} from '../../generated/prisma-client';

interface AddStressDataArg {
  date: string;
  intensity: number;
}

const addStressData = async (parent: any, arg: AddStressDataArg, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  await prisma.createStressData({
    date: arg.date,
    intensity: arg.intensity,
    user: {connect: {id: context.request.user.id}},
  });

  return true;
}

export default {
  addStressData
}
