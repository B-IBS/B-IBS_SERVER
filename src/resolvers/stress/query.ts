import { prisma, StressData } from "../../generated/prisma-client";
import { AuthError } from "../../error/AuthError";

const getStress = async (parent: any, _: null, context: any): Promise<Array<StressData>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  const stress = await prisma.stressDatas({
    where: {
      user: {
        id: context.request.user.id
      }
    },
  });

  return stress
}

export default {
  getStress
}
