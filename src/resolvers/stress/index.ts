import getStress from './query';
import AddStressData from './mutation';

export default {
  Query: { ...getStress },
  Mutation: { ...AddStressData },
};
