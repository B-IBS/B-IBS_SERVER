import {Context} from "graphql-yoga/dist/types";
import {prisma} from "../../generated/prisma-client";
import {AuthError} from "../../error/AuthError";


interface MetricRequestElement {
    name: string;
    count: number;
}

interface MetricRequestLog {
    id: string;
    createdAt: string;
    updatedAt: string;
    name: string;
}

interface MetricRequestStat {
    stats: MetricRequestElement[];
    logs: MetricRequestLog[];
}

/*
 * mutation: register(email: String! password: String! name: String, firstname: String): Authentification!
 * @param: email String! : email of new account.
 * @param: password String! : password of the new account.
 * @param: name String : name of the new account.
 * @param: firstname String : first name of the new account.
 * @return: Boolean : True if the account have been created successfully.
 *
 * Create a new account.
 */
const getMetricsRequestStat = async (parent: never, arg: never, ctx: Context): Promise<MetricRequestStat> => {
    if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
    if (!ctx.request.user.isAdmin) throw new AuthError(401, 'This query needs an admin user');

    const raw_metrics = await prisma.metricsRequests()
    const stats = new Map()
    const metrics_logs = raw_metrics.map((e) => {
        if (stats.has(e.requestName))
            stats.set(e.requestName, stats.get(e.requestName) + 1);
        else
            stats.set(e.requestName, 1);

        return {
            id: e.id,
            createdAt: e.createdAt,
            updatedAt: e.updatedAt,
            name: e.requestName,
        };
    })
    return {stats: Array.from( stats ).map(([key, value]) => ({ name: key, count: value })), logs: metrics_logs}
};

export default {
    getMetricsRequestStat,
}