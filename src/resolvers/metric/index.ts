import getMetricsRequestStat from './requestPopularity'
import getMetricsUser from './user'

export default {
    Query: {
        ...getMetricsRequestStat,
        ...getMetricsUser,
    },
    Mutation: {
    }
};
