import {Context} from "graphql-yoga/dist/types";
import {AuthError} from "../../error/AuthError";
import {prisma, User} from "../../generated/prisma-client";
import groupBy from "../../tools/groupBy";
import * as moment from 'moment';


interface UserScaled {
    date: string;
    number: number;
}

interface MetricUser {
    numberOfUser: number;
    numberOfActiveUser: number;
    scaledData: UserScaled[];
}

/*
 * query: getMetricsUser(scale: String): MetricUser!
 * @param: scale String : allows to retrieve the number of user account creation according to a scale.
 * @return: MetricUser : Metrics around user.
 *
 * Retrive metrics around user.
 */

const getNewIncomerFromScale = async (scale: string, users: User[]): Promise<UserScaled[]> => {
    let grouped_users
    if (scale === "year")
        grouped_users = groupBy(users, user => moment(user.createdAt).year().toString())
    else if (scale === "month")
        grouped_users = groupBy(users, user => moment(user.createdAt).year().toString() + '-' + moment(user.createdAt).month().toString())
    else
        grouped_users = groupBy(users, user => moment(user.createdAt).year().toString() + '-' + moment(user.createdAt).month().toString() + '-' + moment(user.createdAt).week().toString())
    return Array.from(grouped_users).map(([key, value]) => ({date: key, number: value.length}))
}

const getNbActiveuser = async (): Promise<number> => {
    const date = new Date()
    const past_date = date.getDate() - 7
    date.setDate(past_date)
    const lastRequest = await prisma.$graphql(`query { metricsRequests(where: {user: {id_not: null}, createdAt_gt: "${date.toISOString()}"}) {id user {id}}}`)
    const activeUser = lastRequest.metricsRequests.map((e) => e.user.id).filter((e, index, self) => index === self.indexOf(e))
    return activeUser.length
}

const getMetricsUser = async (parent: never, {scale}: { scale: string }, ctx: Context): Promise<MetricUser> => {
    if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
    if (!ctx.request.user.isAdmin) throw new AuthError(401, 'This query needs an admin user');

    const users = await prisma.users()

    const a: MetricUser = {
        numberOfUser: users.length,
        numberOfActiveUser: await getNbActiveuser(),
        scaledData: await getNewIncomerFromScale(scale, users)
    }
    return a
}

export default {
    getMetricsUser
}