import submitReport from './submitReport';

export default {
  Query: {
  },
  Mutation: {
    ...submitReport,
  },
};
