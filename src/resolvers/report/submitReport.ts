import { prisma } from '../../generated/prisma-client';

interface SubmitReportArg {
  user_info: string;
  report_type: string;
  system_info: string | null;
}

/*
 * mutation: submitReport(user_info: String! report_type: String! system_info: String): Boolean!
 * @param: user_info String! : user feed back.
 * @param: report_type String! : Type of error
 * @param system_info String! : Id of the error to link with
 * @return: Boolean : return True if the report is correctly submit.
 *
 * Create a new report witch can be linked to a server error.
 */
const submitReport = async (parent: any, arg: SubmitReportArg, ctx: any): Promise<boolean> => {
  const report = await prisma.createReport({ userInfo: arg.user_info, reportType: arg.report_type });
  if (arg.system_info) {
    const error = prisma.errorLog({ id: arg.system_info });
    if (error) {
      await prisma.updateReport({
        where: { id: report.id },
        data: { system_info: { connect: { id: arg.system_info } } },
      });
    }
  }
  if (ctx.request.user) {
    await prisma.updateReport({
      where: { id: report.id },
      data: { user: { connect: { id: ctx.request.user.id } } },
    });
  }
  return true;
};

export default {
  submitReport,
};
