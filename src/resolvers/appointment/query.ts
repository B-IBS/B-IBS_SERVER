import {prisma} from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';


interface Resume {
  id: string;
  updatedAt: string;
  appointment: string;
  editor: string;
  note: string;
}

/*
 * query: getResume(date: DateTime date_filter: DateFilter, max_result: Int appointment: String id: String): [Resume]!
 * @param: date DateTime : date that will be taken at end or starting point depends on date_filter
 * @param: date_filter DateFilter! : determines if the returned value is: since date or until date
 * @param: max_result Int : maximum number of result if not specify all is return
 * @param: appointment String : id of appointment
 * @param: id String : id of the wanted resume
 * @return list of resume
 *
 * Return a list of resume depend on filter,id and if there is no id, the filter order is appointment, date and then max_result.
 * The date filtering is apply on the date attribute.
 */

const getResume = async (parent, arg, ctx): Promise<Resume[]> => {
  let result;
  let numberOfResult;

  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  if (arg.id) {
    result = [await prisma.appointment({ id: arg.id })];
  } else {
    if (arg.appointment) result = await prisma.$graphql(`query { resumes(where: { appointment: { user: {id: "${ctx.request.user.id}"} id: "${arg.appointment}"}}) {updatedAt editor id note createdAt appointment {id}}}`);
    else result = await prisma.$graphql(`query { resumes(where: { appointment: { user: {id: "${ctx.request.user.id}"}}}) {updatedAt editor id note createdAt appointment {id}}}`);
    result = result.resumes;

  }
  if (arg.date && arg.date_filter) {
    if (arg.date_filter === 'UNTIL') result = result.filter((element) => new Date(element.updatedAt) < new Date(arg.date));
    else result = result.filter((element) => new Date(element.updatedAt) > new Date(arg.date));
  }

  if (arg.max_result) {
    numberOfResult = (arg.max_result > result.length) ? result.length : arg.max_result;
    result = result.slice(0, numberOfResult);
  }

  result = result.map((element) => ({
    id: element.id,
    updatedAt: element.updatedAt,
    appointment: element.appointment.id,
    editor: element.editor,
    note: element.note,
  }));
  return result;
};

interface Appointment {
  id: string;
  updatedAt: string;
  date: string;
  wiith: string;
  at: string;
  resumes: string;
}

/*
 * query: getAppointment(date: DateTime date_filter: DateFilter, max_result: Int, id: String): [Appointment]!
 * @param: date DateTime : date that will be taken at end or starting point depends on date_filter
 * @param: date_filter DateFilter! : determines if the returned value is: since date or until date
 * @param: max_result Int : maximum number of result if not specify all is return
 * @param: id String : Id of the wanted resume
 * @return list of appointment
 *
 * Return a list of resume depend on filter, id and if there is no id, date and then max_result.
 * The date filtering is apply on the updatedAt attribute.
 */

const getAppointment = async (parent, { date, dateFilter, maxResult, id, }, ctx): Promise<Appointment[]> => {
  let result;
  let numberOfResult;

  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  if (id) {
    result = [await prisma.appointment({ id })];
  } else {
    result = await prisma.$graphql(`query { appointments(where: {user: {id: "${ctx.request.user.id}"}}) {id createdAt updatedAt date user {id} wiith at {latitude longitude raw}}}`);
    result = result.appointments;
    if (date && dateFilter) {
      if (dateFilter === 'UNTIL') result = result.filter((element) => new Date(element.date) < new Date(date));
      else result = result.filter((element) => new Date(element.date) > new Date(date));
    }
  }

  /*
  id: ID!
    updatedAt: String!
    appointment: String!
    editor: String!
    note: String!
   */
  let resumes;
  for (const element of result) {
    resumes = await prisma.resumes({ where: { appointment: { id: element.id } } });
    resumes = resumes.map((resume) => {
      return {id: resume.id, updatedAt: resume.updatedAt, appointment: resume.appointment, note: resume.note}
    });
    element.resumes = resumes;
  }

  if (maxResult) {
    numberOfResult = (maxResult > result.length) ? result.length : maxResult;
    result = result.slice(0, numberOfResult);
  }

  result = result.map((element) => ({
    id: element.id,
    updatedAt: element.updatedAt,
    date: element.date,
    wiith: element.wiith,
    at: element.at,
    resumes: element.resumes,
  }));
  return result;
};

export default {
  getResume,
  getAppointment,
};
