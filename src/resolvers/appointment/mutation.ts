import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';
import access from '../../config/access';
import MyError from '../../error/MyError';

/*
 * mutation: createAppointment(date: DateTime! wiith: String at: String): String!
 * @param: date DateTime! : Date were the appointment is set
 * @param: wiith String : Name of the doctor
 * @param: at String : Where the appoint is set
 * @return: String : If the creation is successful it return the id of the new element or return a error in case of error
 *
 * Create a new appointment
 */
const createAppointment = async (parent, arg, ctx): Promise<string> => {
  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  const appointment = await prisma.createAppointment({
    date: new Date(arg.date),
    user: { connect: { id: ctx.request.user.id } },
  });

  if (arg.wiith) await prisma.updateAppointment({ where: { id: appointment.id }, data: { wiith: arg.wiith } });
  if (arg.at) {
    const { lat, long } = arg.at.split(' ');
    await prisma.updateAppointment({
      where: { id: appointment.id },
      data: { at: { create: { latitude: lat, longitude: long } } },
    });
  }
  return appointment.id;
};

/*
 * mutation: createResume(appointment: String! note: String!): String!
 * @param: appointment String! : Appointment id to connect with
 * @param: note String! : Resume data
 * @return: String : If the creation is successful it return the id of the new element or return a error in case of error
 *
 * Create a new resume and link connect it with the correct appointment
 */
const createResume = async (parent, arg, ctx): Promise<string> => {
  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  let resume;
  if (ctx.request.user && ctx.request.user.id)
    resume = await prisma.createResume({ editor: 'USER', appointment: { connect: { id: arg.appointment } }, note: arg.note });
  else
    resume = await prisma.createResume({ editor: 'DOCTOR', appointment: { connect: { id: arg.appointment } }, note: arg.note });
  return resume.id;
};

/*
 * mutation: updateResume(id: String! note: String!): Boolean!
 * @param: id String! : Resume id to modify
 * @param: note String! : Resume data
 * @return: Boolean : True if the creation is successful or return a error in case of error
 *
 * Update Resume data
 */
const updateResume = async (parent, arg, ctx): Promise<boolean> => {
  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  const previous = await prisma.$graphql(`
  query {
    resume(where: { id: "${arg.id}" }) {id appointment {id}}
  }
`);
  if (!previous) throw new MyError(404, `Could not find resume number${arg.id}`, 'Resume not found error');
  if (ctx.request.user.authorisation === access.OWNER) await prisma.updateResume({ where: { id: arg.id }, data: { editor: 'USER', appointment: { connect: { id: previous.resume.appointment.id } }, note: arg.note } });
  else await prisma.updateResume({ where: { id: arg.id }, data: { editor: 'DOCTOR', appointment: { connect: { id: previous.resume.appointment.id } }, note: arg.note } });
  return true;
};

export default {
  createAppointment,
  createResume,
  updateResume,
};
