import getPartnerCodes from './getter';

export default {
  Query: {
    ...getPartnerCodes,
  },
  Mutation: {
  },
};
