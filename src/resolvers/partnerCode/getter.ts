import { prisma } from '../../generated/prisma-client';
import { adminMiddleware } from '../../middleware/authentification';

/*
 * query: getPartnerCodes: [String]!
 * @return: [String]! : The list of all the partner code.
 *
 * Return the list of all the partner code.
 */
const getPartnerCodes = async (): Promise<string[]> => {
  const a = await prisma.partnerCodes();
  return a.map((e) => e.code);
};

export default {
  getPartnerCodes: adminMiddleware(getPartnerCodes),
};
