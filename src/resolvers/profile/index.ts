import addProfile from './addProfile';
import updateProfile from './updateProfile';
import getUser from './getUser';
import getUserStat from './getUserStat';

export default {
  Query: {
    ...getUser,
    ...getUserStat,
  },
  Mutation: {
    ...addProfile,
    ...updateProfile,
  },
};
