import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

interface UpdateProfileArg {
  sex: string;
  height: string;
  weight: string;
  smoke: boolean;
  treatment: boolean;

}

/*
 * mutation: updateProfile(date: String! intensity: String! report: String): Boolean!
 * @param: sex: String       : New sex
 * @param: height: String    : New height
 * @param: weight: String    : New weight
 * @param: smoke: Boolean    :
 * @param: treatment:Boolean :
 * @return: Boolean!         : If Profile was correctly update
 *
 * update profile for a User
 */

const updateProfile = async (parent: any, arg: UpdateProfileArg, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  const fcUser = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  if (fcUser.length === 0) return false;
  await prisma.updateProfil(
    {
      data: {
        sex: arg.sex,
        height: arg.height,
        weight: arg.weight,
        smoke: arg.smoke,
        treatment: arg.treatment,
      },
      where:
           {
             id: fcUser[0].id,
           },
    },
  );
  return true;
};

export default {
  updateProfile,
};
