import { prisma } from '../../generated/prisma-client';
import {NoneMetricsAuthError} from "../../error/AuthError";

/*
 * query: query/mutation prototype
 * @param: foo Type :/.
 * @return: Type : Returns all statistics.
 *
 * The query retrieve all data and returns.
 */


const getUserStat = async (parent: any, _: any, context: any): Promise<{ createdAt: any; nbCrise: any; moyCrise: any; nbMedicines: any; updatedAt: any }> => {
  if (!context.request.user) throw new NoneMetricsAuthError(401, 'This query need a connected user');
  const profil = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  const stat = {
    updatedAt: profil[0].updatedAt,
    createdAt: profil[0].createdAt,
    nbCrise: profil[0]?.nb_crisis || 0,
    moyCrise: profil[0]?.moy_crisis || 0,
    nbMedicines: profil[0]?.nb_medicine || 0,
    dayWithoutCrisis: profil[0]?.day_without_crisis || 0,
  };
  return (stat);
};

export default {
  getUserStat,
};
