import {prisma, Profil, User} from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

/*
 * mutation: addCrisis(date: String! intensity: String! report: String): Boolean!
 * @param: date String!      : Date the crisis happened
 * @param: intensity String! : Intensity of the crisis
 * @param: report String!    : Description of the crisis
 * @return: Boolean!         : If Crisis was correctly added
 *
 * Create a new Crisis for a User
 */
// Wrong documentation
// Wrong naming this more a get Profile that get User
const getUser = async (parent: any, _: any, context: any): Promise<Partial<Profil & User>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  const user = await prisma.user({ id: context.request.user.id });
  const profil = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  return {
    ...user,
    ...(
      profil.length > 0 ? 
      profil[0] : {}
    )
  };
};
export default {
  getUser,
};
