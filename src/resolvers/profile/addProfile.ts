import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

/*
 * mutation: addCrisis(date: String! intensity: String! report: String): Boolean!
 * @param: date String!      : Date the crisis happened
 * @param: intensity String! : Intensity of the crisis
 * @param: report String!    : Description of the crisis
 * @return: Boolean!         : If Crisis was correctly added
 *
 * Create a new Crisis for a User
 */

interface AddProfilArgs {
  sex: string | null;
  height: string | null;
  weight: string | null;
  smoke: boolean;
  treatment: boolean;
}

const addProfile = async (_: any, arg: AddProfilArgs, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  const user = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  if (user.length >= 1) throw new AuthError(401, 'this user has already been created');
  await prisma.createProfil({
    day_without_crisis: 0,
    nb_medicine: 0,
    moy_crisis: 0,
    som_crisis: 0,
    nb_crisis: 0,
    nb_meal: 0,
    user: { connect: { id: context.request.user.id } },
    sex: arg.sex,
    height: arg.height,
    weight: arg.weight,
    smoke: arg.smoke,
    treatment: arg.treatment,
  });
  return true;
};

export default {
  addProfile,
};
