import {Context} from "graphql-yoga/dist/types";
import {AuthError} from "../../error/AuthError";
import axios from "axios";
import {prisma} from "../../generated/prisma-client";


interface User_type {
    id: string,
    authorisation: number,
}

interface Ingredient {
    label: string;
    weight: number;
    foodCategory: string;
    img: string;
}

interface Recipe {
    label: string;
    url: string;
    calories: number;
    img: string;
    ingredients: [Ingredient]
    intolerant: [{name: string, id: string}]
}

const fetchRecipe = async (key_word: string): Promise<Recipe[]> => {
    try {
        const response = await axios.get('https://api.edamam.com/api/recipes/v2?q=' + key_word + '&app_id=' + process.env.EDAMAM_ID + '&app_key=' + process.env.EDAMAM_KEY + '&type=public&random=true');
        return response.data.hits.map((recipe) => {
            const ingredients = recipe.recipe.ingredients.map((ingredient) => {
                return {
                    label: ingredient.text,
                    weight: ingredient.weight,
                    foodCategory: ingredient.foodCategory,
                    img: ingredient.image,
                }
            })
            return {
                label: recipe.recipe.label,
                url: recipe.recipe.url,
                calories: recipe.recipe.calories,
                img: recipe.recipe.image,
                ingredients: ingredients,
                intolerant: []
            }
        });
    } catch (error) {
        console.error(error);
        return []
    }
}

interface UserFood {
    id: string,
    level: number,
    food: {
        name: string,
        id: string
    }
}

const fetchUserIntolerantFood = async (user: User_type): Promise<UserFood[] | []> => {
    return await Promise.all(
        (
            await prisma.$graphql(`query { userFoods(where: {user: {id: "${user.id}"}}) {id level food {id name}}}`)
        ).userFoods.filter(x => x.level > 0).map(x => {
            const res: UserFood = {id: x.id, level: x.level, food: {name: x.food.name, id: x.food.id}}
            return res
        })
    )
}

interface RecipeArg {
    key_word: string;
}

/*
 * query: getRecipe(key_word: String!): Boolean!
 * @param: key_word String! : key word used to search recipe which use this key word.
 * @param: password String! : password of the user.
 * @return: Authentification : return a connection token.
 *
 * return a token that identify the user.
 */
const getRecipe = async (_: never, arg: RecipeArg, context: Context): Promise<Recipe[] | []> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    const fetched_recipe = await fetchRecipe(arg.key_word);
    const intolerant_user_food = await fetchUserIntolerantFood(context.request.user)
    for (const recipe of fetched_recipe) {
        for (const ingredient of recipe.ingredients) {
            for (const intolerant_food of intolerant_user_food) {
                if (ingredient.label.toLowerCase().includes(intolerant_food.food.name.toLowerCase())) {
                    recipe.intolerant.push({name: intolerant_food.food.name, id: intolerant_food.id})
                }
            }
        }
    }
    return fetched_recipe;
};

export default {
    getRecipe,
};


