import getRecipe from "./getRecipe";

export default {
    Query: {
        ...getRecipe,
    },
    Mutation: {
    },
};
