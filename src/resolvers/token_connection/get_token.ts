import * as jwt from 'jsonwebtoken';
import { AuthError } from '../../error/AuthError';
import { prisma } from '../../generated/prisma-client';
import access from '../../config/access';
import { jwtKey } from '../../config/secret_key';
import expiresIn from '../../config/Jwt';


interface SmallToken {
  token: string;
  expiration_date: string;
}

/*
 * query: connectionToken: Token_connection!
 * @return: Token_connection : connection token with it's expiration date
 *
 * Return the current small token
 */
const connectionToken = async (parent: unknown, _: any, ctx: any): Promise<SmallToken> => {
  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  const token = await prisma.tokenConnections({ where: { user: { id: ctx.request.user.id } } });
  if (token.length === 0) {
    return {
      token: 'null',
      expiration_date: 'null',
    };
  }
  return {
    token: token[0].token,
    expiration_date: token[0].expiration_date,
  };
};

interface LogWithSmallTokenArg {
  token: string;
}

/*
 * query: logWithSmallToken(token: String!): Authentification!
 * @param: token String!: small token
 * @return: Authentification : Auth token
 *
 * Return a acces token with DOCTOR right
 */
const logWithSmallToken = async (parent: unknown, arg: LogWithSmallTokenArg, _: unknown):
Promise<LogWithSmallTokenArg> => {
  const { tokenConnections } = await prisma.$graphql(`query { tokenConnections(where: {token: "${arg.token}"}) {user {id} expiration_date}}`);
  if (tokenConnections.lenght === 0) throw new AuthError(400, 'Small token invalid');
  const jwTo = jwt.sign({ id: tokenConnections[0].user.id, authorisation: access.DOCTOR }, jwtKey,
    {
      algorithm: 'HS256',
      expiresIn,
    });
  return {
    token: jwTo,
  };
};

export default {
  connectionToken,
  logWithSmallToken,
};
