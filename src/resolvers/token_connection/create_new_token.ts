import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

const makeid = (length): string => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

interface CreateConnectionTokenArg {
  expiration_date: string;
}

interface SmallToken {
  token: string;
  expiration_date: string;
}

/*
 * mutation: createConnectionToken(expiration_date: DateTime!): Token_connection!
 * @param: expiration_date DateTime! : Date were the token will expire.
 * @return: Token_connection : connection token with it's expiration date.
 *
 * Create a new read only connection token
 */
const createConnectionToken = async (parent: any, arg: CreateConnectionTokenArg, ctx: any): Promise<SmallToken> => {
  if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
  await prisma.deleteManyTokenConnections({ user: { id: ctx.request.user.id } });
  const tokenValue = makeid(4);
  await prisma.createTokenConnection({
    expiration_date: new Date(arg.expiration_date),
    user: { connect: { id: ctx.request.user.id } },
    token: tokenValue,
  });
  return {
    token: tokenValue,
    expiration_date: arg.expiration_date,
  };
};

export default {
  createConnectionToken,
};
