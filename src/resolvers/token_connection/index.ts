import createToken from './create_new_token';
import getToken from './get_token';

export default {
  Query: {
    ...getToken,
  },
  Mutation: {
    ...createToken,
  },
};
