import endpoint from './endPoints'

export default {
    Query: {
        ...endpoint.Query
    },
    Mutation: {
        ...endpoint.Mutation
    },
};
