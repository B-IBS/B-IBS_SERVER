import {prisma} from '../../generated/prisma-client';

const getPastFood = async (user: string, crisisDate: Date): Promise<any> => {
    crisisDate.setDate(crisisDate.getDate() - 2)
    const pastMeal = await prisma.$graphql(`
    query {
        meals(where: {user: {id: "${user}"}, date_gt: "${crisisDate.toISOString()}"}) {food {id}}
    }
    `)
    let mealFood = [];
    for (const item of pastMeal.meals) {
        mealFood = mealFood.concat(item.food)
    }
    return Array.from(new Set(mealFood.map(a => a.id)))
        .map(id => {
            return mealFood.find(a => a.id === id)
        })
}

interface userProximity {
    id: string;
    proximity: number;
    level: number;
}

const calculateProximity = async (user: string, userList: Array<string>, userSusFood: Array<any>): Promise<Array<userProximity>> => {
    const proximityList = []
    for (const linkedUser of userList) {
        const filter = []
        userSusFood.map((item) => {
            filter.push({
                food: {
                    id: item.food.id
                }
            })
        })
        //console.log("FILTER")
        //console.log(filter)
        //console.log(linkedUser)
        const commonSusFood = await prisma.suspiciousFoodses({where: {user: {id: linkedUser}, OR: filter}})
        if (commonSusFood.length === 0)
            return []
        let proximity = 0
        commonSusFood.map((item) => {
            proximity = proximity + item.level
        })
        //console.log(commonSusFood)
        //console.log(proximity)
        //console.log(commonSusFood.length)
        proximityList.push({
            id: linkedUser,
            proximity: proximity,
            level: proximity / commonSusFood.length
        })
    }
    return proximityList
}

const createSuspiciousFood = async (user: string, susFood: any, closestUser: Array<userProximity>): Promise<void> => {
    let level = 0
    if (closestUser.length == 0)
        level = 15
    else {
        for (const linkedUser of closestUser)
            level = level + linkedUser.level
        level = (level / closestUser.length) / 2
    }
    //console.log("SUSFOOD = ")
    //console.log(susFood)
    const exist = await prisma.suspiciousFoodses({where: {food: {id: susFood.id}, user: {id: user}}})
    if (exist.length === 0) {
        //console.log("NEW")
        //console.log(level)
        await prisma.createSuspiciousFoods({
            user: {
                connect: {
                    id: user
                }
            },
            food: {
                connect: {
                    id: susFood.id
                }
            },
            level: level > 99 ? 99 : Math.round(level)
        })
    } else {
        //console.log("PAS NEW")
        //console.log(exist)
        //console.log(level)
        if (closestUser.length != 0)
            level = (exist[0].level + Math.round(level / closestUser.length))
        else
            level = exist[0].level + level
        //console.log(level)
        await prisma.updateSuspiciousFoods({
            where: {id: exist[0].id}, data: {
                level: level > 99 ? 99 : Math.round(level)
            }
        })
    }
}

const updateFromSusFood = async (user: string, susFood: any): Promise<void> => {
    const userSusFood = await prisma.$graphql(`query {
  suspiciousFoodses(where: {user: {id: "${user}"}}) {food {id}}
}`)
    for (const food of susFood) {
        //console.log(food)
        const payload = await prisma.$graphql(`
            query {
                suspiciousFoodses(where: {food: {id: "${food.id}"} level_gte: 50}) {user {id}}
            }
        `)
        const userList = []
        for (const item of payload.suspiciousFoodses)
            userList.push(item.user.id)
        let linkedUser = await calculateProximity(user, userList, userSusFood.suspiciousFoodses)
        //if (linkedUser.length != 0)
            //console.log(linkedUser)
        //else
            //console.log("No linked user")
        linkedUser = linkedUser.sort((a, b) => b.proximity - a.proximity).slice(0, 10)
        await createSuspiciousFood(user, food, linkedUser)
        //console.log("\n")
    }
}

const updateGraph = async (user: string, crisisDate: Date): Promise<void> => {
    const pastFoods = await getPastFood(user, crisisDate)
    //console.log(pastFoods)
    //console.log("\n")
    await updateFromSusFood(user, pastFoods)
}
export default updateGraph