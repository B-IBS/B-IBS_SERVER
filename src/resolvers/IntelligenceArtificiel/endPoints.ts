import {Context} from "graphql-yoga/dist/types";
import {AuthError} from "../../error/AuthError";
import {prisma} from "../../generated/prisma-client";


interface Food {
    id: string,
    food: string,
    level: number,
    updatedAt: string
}

/*
 * query: getSuspiciousFood: [SuspiciousFood]!
 * @return: [SuspiciousFood]! : List of suspicious food.
 *
 * Return the list of the suspicious food.
 */
const getSuspiciousFood = async (parent: never, __: never, context: Context): Promise<Array<Food>> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    const payload = await prisma.$graphql(`query {
        suspiciousFoodses(where: {user: {id: "${context.request.user.id}"} level_gte: 50}) {id food {name} level updatedAt}
    }`)
    const suspiciousFoods = []
    payload.suspiciousFoodses.map((item) => {
        if (item.level < 100)
            suspiciousFoods.push({
                id: item.id,
                food: item.food.name,
                level: item.level,
                updatedAt: item.updatedAt
            })
    })
    return suspiciousFoods
};

/*
 * query: getFlaggedFood: [SuspiciousFood]!
 * @return: [SuspiciousFood]! : List food flagged by the AI.
 *
 * Return the list food flagged by the AI.
 */
const getFlaggedFood = async (parent: never, __: never, context: Context): Promise<Array<Food>> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    const payload = await prisma.$graphql(`query {
        suspiciousFoodses(where: {user: {id: "${context.request.user.id}"} level: 100}) {id food {name} level updatedAt}
    }`)
    const flaggedFood = []
    payload.suspiciousFoodses.map((item) => {
        flaggedFood.push({
            id: item.id,
            food: item.food.name,
            level: item.level,
            updatedAt: item.updatedAt
        })
    })
    return flaggedFood
};


/*
 * mutation: flagNewFood(id: String!, value: Int!): Boolean
 * @param: id: String food id to change
 * @param: value: Int value of the modification
 * @return: Boolean: true if success.
 *
 * Confirm or not a new flagged food.
 */
const flagNewFood = async (parent: never, arg: any, context: Context): Promise<boolean> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    const exist = await prisma.suspiciousFoods({id: arg.id})
    if (!exist)
        return false
    if (arg.value == 0)
        await prisma.updateSuspiciousFoods({where: {id: arg.id}, data: {level: 100}})
    else if (arg.value == 1)
        await prisma.updateSuspiciousFoods({where: {id: arg.id}, data: {level: 0}})
    return true
};

export default {
    Query: {
        getSuspiciousFood,
        getFlaggedFood,
    },
    Mutation: {
        flagNewFood
    },
};
