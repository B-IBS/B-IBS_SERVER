import {Meal, Food, prisma, ScannedProduct} from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

/*
 * query: getMeal: [Meal]!
 * @return: [Meal] : return the list of the meal enter by the user.
 *
 * return the list of the meal enter by the user.
 */


interface ResultMeal extends Meal {
  food: Array<Food>;
}

const getMeal = async (parent: any, _: null, context: any): Promise<Array<ResultMeal>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  const meals = await prisma.meals({
    where: {
      user: {
        id: context.request.user.id
      }
    },
  });

  const mealsWithFood = await Promise.all(
    meals.map(async (m) => {
      return {
        food: await prisma.meal({ id: m.id }).food(),
        ...m,
      }
    })
  );
  return mealsWithFood;
};

/*
 * query: getScannedProduct: [ScannedProduct]!
 * @return: [ScannedProduct] : return the list of the scanned product enter by the user.
 *
 * return the list of the scanned product enter by the user.
 */

const getScannedProduct = async (parent: any, _: null, context: any): Promise<ScannedProduct[]> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  return prisma.scannedProducts({ where: { user: { id: context.request.user.id } } });
};

/*
 * query: searchPastMeal(keyword: String): [Meal]!
 * @param: keyword: Searching keyword
 * @return: [ScannedProduct] : return the list of the meal enter by the user that have the keyword in their name.
 *
 * return the list of the meal enter by the user that have the keyword in their name.
 */

const searchPastMeal = async (parent: any, {keyword}: {keyword: string}, context: any): Promise<Array<ResultMeal>> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  const meals = await prisma.meals({
    where: {
      user: {
        id: context.request.user.id
      },
      name_contains: keyword
    }
  })

  return await Promise.all(
      meals.map(async (m) => {
        return {
          food: await prisma.meal({ id: m.id }).food(),
          ...m,
        }
      }).slice(0, 20)
  );
}



export default {
  getMeal,
  getScannedProduct,
  searchPastMeal,
};
