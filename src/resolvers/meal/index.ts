import getMeal from './query';
import AddMeal from './mutation';

export default {
  Query: { ...getMeal },
  Mutation: { ...AddMeal },
};
