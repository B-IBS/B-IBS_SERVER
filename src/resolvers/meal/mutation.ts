import {AuthError} from '../../error/AuthError';
import {prisma} from '../../generated/prisma-client';

import {InvalidArgumentError} from '../../error/AuthError';

/*
* mutation: AddMeal(meal: String!): Boolean!
* @param: meal String! : Meal name.
* @return: Boolean : True if no error.
*
* Add a new meal.
*/

interface AddMealArg {
    name: string;
    food: Array<string>;
    date: string;
    notes: string;
}

const addMeal = async (parent: any, arg: AddMealArg, context: any): Promise<boolean> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    await prisma.createMeal({
        name: arg.name,
        food: {
            connect: arg.food.map((e) => ({id: e}))
        },
        date: arg.date,
        notes: arg.notes,
        user: {connect: {id: context.request.user.id}},
    });
    const profil = await prisma.profils({where: {user: {id: context.request.user.id}}});
    if (profil.length) {
        await prisma.updateProfil({
            data: {
                nb_meal: profil[0].nb_meal + 1
            },
            where: {
                id: profil[0].id
            }
        });
    } else {
        await prisma.createProfil({
            day_without_crisis: 0,
            nb_medicine: 0,
            moy_crisis: 0,
            som_crisis: 0,
            nb_crisis: 0,
            nb_meal: 1,
            user: {connect: {id: context.request.user.id}},
        });
    }
    return true;
};

/*
* mutation: AddScannedMeal(name: String! foodmapLevel: Int! productId: String!): Boolean!
* @param: name String! : Meal id.
* @param: foodmapLevel Int! : Level of food map.
* @param: productId String! : id return by the barcode scan API.
* @return: Boolean : True if no error.
*
* Add a scanned meal.
*/

interface AddScannedMealArg {
    name: string;
    foodmapLevel: number;
    productId: string;
    rawData: string,
}

const AddScannedProduct = async (parent: any, arg: AddScannedMealArg, context: any): Promise<boolean> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    const rawData = JSON.parse(arg.rawData);
    const exist = await prisma.$graphql(`query { scannedProducts(where: {productId: "${arg.productId}"}) {food {id}}}`)
    let food_id;
    if (exist.scannedProducts.length == 0) {
        const food = await prisma.createFood({
            name: arg.name,
            level: arg.foodmapLevel,
            imageUrl: rawData.product.image_front_small_url,
            details: '',
            caution: false,
        });
        food_id = food.id;
    } else
        food_id = exist.scannedProducts[0].food.id;
    await prisma.createScannedProduct({
        user: {connect: {id: context.request.user.id}},
        foodmapLevel: arg.foodmapLevel,
        name: arg.name,
        productId: arg.productId,
        food: {connect: {id: food_id}}
    })
    return true;
};

/*
* mutation: RemoveMeal(id: String!): Boolean!
* @param: id String! : Meal id.
* @return: Boolean : True if no error.
*
* Remove an meal.
*/

interface RemoveMealArg {
    id: string;
}

const RemoveMeal = async (parent: any, arg: RemoveMealArg, context: any): Promise<boolean> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    await prisma.deleteMeal({id: arg.id});
    return true;
};

interface UpdateMealArg extends AddMealArg {
    id: string;
}

const updateMeal = async (_: any, arg: UpdateMealArg, __: any): Promise<boolean> => {
    const exist = await prisma.meal({id: arg.id});

    if (!exist) throw new InvalidArgumentError(409, "Ce repas n'existe pas/plus.");

    await prisma.updateMeal({
        where: {id: arg.id},
        data: {
            name: arg.name,
            food: {
                connect: arg.food.map((e) => ({id: e}))
            },
            date: arg.date,
            notes: arg.notes,
        },
    });
    return true;
};

export default {
    addMeal,
    RemoveMeal,
    AddScannedProduct,
    updateMeal,
};

interface AddMealArg {
    mealId: string;
    name: string;
    food: Array<string>;
    date: string;
    notes: string;
}
