import login from './login';
import register from './register';
import CGU from './CGU'

export default {
  Query: {
    ...login,
    ...CGU,
  },
  Mutation: {
    ...register,
  },
};
