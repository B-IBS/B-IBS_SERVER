import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import { Context } from 'graphql-yoga/dist/types';

import { generateRefreshToken, getRefreshToken } from '../../tools/refreshToken/resfreshToken';
import { prisma, User } from '../../generated/prisma-client';
import { jwtKey } from '../../config/secret_key';
import access from '../../config/access';
import expiresIn from '../../config/Jwt';
import { AuthError } from '../../error/AuthError';

const getUserFromEmailPassword = async (email: string, password: string): Promise<User | null> => {
  const user = await prisma.user({ email: email.toString() });
  if (!user) { return null; }
  const valid = await bcrypt.compare(password, user.password);
  if (!valid) { return null; }
  return user;
};

interface LoginArg {
  email: string;
  password: string;
}

interface Authentification {
  token: string;
  refreshToken: string;
}

/*
 * query: login(email: String! password: String!): Authentification!
 * @param: email String! : email of the user.
 * @param: password String! : password of the user.
 * @return: Authentification : return a connection token.
 *
 * return a token that identify the user.
 */
const login = async (parent: never, arg: LoginArg, context: Context): Promise<Authentification> => {
  const user = await getUserFromEmailPassword(arg.email, arg.password);
  if (!user) {
    throw new AuthError(400, "Le mot de passe ou l'adresse mail est incorrect");
  }
  // TO DO Mettre un place un vrais système d'authorisation
  const token = jwt.sign({ id: user.id, authorisation: access.OWNER }, jwtKey, {
    algorithm: 'HS256',
    expiresIn,
  });
    // Remove cookie based response ?
  context.response.cookie('token', token, { maxAge: expiresIn * 1000 });
  return {
    token,
    refreshToken: await getRefreshToken({ id: user.id }),
  };
};

interface ForgotPasswordArg {
  email: string;
}

/*
 * query: forgotPassword(email: String!): Boolean!
 * @param: email String! : email of the forgotten password.
 * @return: Boolean : True if the email have correctly be send.
 *
 * Send a email to reset the password.
 */
// TODO
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const forgotPassword = (parent: never, arg: ForgotPasswordArg, context: Context): boolean => true;

interface RefreshTokenLoginArg {
  refreshToken: string;
}

/*
 * query: loginRefresh(refreshToken: String!): Authentification!
 * @param: refreshToken: String! : refresh token to log with
 * @return: Authentification : return a connection token.
 *
 * Connect a user thank to a refresh token.
 */
const loginRefresh = async (parent: never, arg: RefreshTokenLoginArg, context: Context): Promise<Authentification> => {
  const refreshTokenQuery = await prisma.$graphql(`
    query { 
      refreshToken(where: {refreshToken: "${arg.refreshToken}"}) {id user {id} refreshToken}
    }
  `);
  if (!refreshTokenQuery.refreshToken) {
    throw new AuthError(400, 'Refresh token introuvable');
  }
  try {
    await jwt.verify(arg.refreshToken, jwtKey);
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) throw new AuthError(400, 'Refreshtoken expiré');
    else throw new AuthError(400, 'Invalid token');
  }
  const token = jwt.sign(
    {
      id: refreshTokenQuery.refreshToken.user.id,
      authorisation: access.OWNER,
    },
    jwtKey, {
      algorithm: 'HS256',
      expiresIn,
    },
  );
  // Remove cookie based response ?
  context.response.cookie('token', token, { maxAge: expiresIn * 1000 });
  return {
    token,
    refreshToken: await generateRefreshToken({ id: refreshTokenQuery.refreshToken.user.id }),
  };
};

export default {
  login,
  forgotPassword,
  loginRefresh,
};
