import {Context} from "graphql-yoga/dist/types";
import expiresIn from "../../config/Jwt";
import * as jwt from 'jsonwebtoken';
import {jwtKey} from "../../config/secret_key";


/*
 * query: validateCGU: Boolean
 * @return: Boolean : true is the validation is a success.
 *
 * Add a cookie that certificate that the user have validate the CGU.
 */
const validateCGU = async (_: never, __: any, context: Context): Promise<boolean> => {
    const ip = context.request.ip
    const token = jwt.sign(
        {
            id: ip,
            date: Date.now(),
        },
        jwtKey, {
            algorithm: 'HS256',
            expiresIn,
        },
    );
    context.response.cookie('BIBS_CGU', token, {
        maxAge: 1000 * 24 * 60 * 60 * 7
    });
    return true
}

export default {
    validateCGU
}