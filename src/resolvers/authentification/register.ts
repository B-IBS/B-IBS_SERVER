import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import { Context } from 'graphql-yoga/dist/types';

import { prisma } from '../../generated/prisma-client';
import { jwtKey } from '../../config/secret_key';
import expiresIn from '../../config/Jwt';
import access from '../../config/access';
import { NoneMetricsAuthError } from '../../error/AuthError';
import { getRefreshToken } from '../../tools/refreshToken/resfreshToken';

interface Authentification {
  token: string;
  refreshToken: string;
}
interface RegisterArgs {
  email: string;
  password: string;
  name: string | null;
  firstname: string | null;
  partnerCode: string | null;
}
/*
 * mutation: register(email: String! password: String! name: String, firstname: String): Authentification!
 * @param: email String! : email of new account.
 * @param: password String! : password of the new account.
 * @param: name String : name of the new account.
 * @param: firstname String : first name of the new account.
 * @return: Boolean : True if the account have been created successfully.
 *
 * Create a new account.
 */
const register = async (parent: never, arg: RegisterArgs, context: Context): Promise<Authentification> => {
  const exist = await prisma.user({ email: arg.email.toString() });
  if (exist) throw new NoneMetricsAuthError(409, 'Cette email est déjà utilisé');
  if (arg.partnerCode) {
    const partner = await prisma.partnerCode({ code: arg.partnerCode });
    if (!partner) throw new NoneMetricsAuthError(410, `${arg.partnerCode} partner code do not exist`);
  }
  const user = await prisma.createUser({
    ...arg,
    password: await bcrypt.hash(arg.password, 10),
  });
  await prisma.createFireBaseID({ user: { connect: { id: user.id } } });
  const token = jwt.sign({ id: user.id, authorisation: access.OWNER }, jwtKey, {
    algorithm: 'HS256',
    expiresIn,
  });
  context.response.cookie('token', token, { maxAge: expiresIn * 1000 });
  return {
    token,
    refreshToken: await getRefreshToken({ id: user.id }),
  };
};

export default {
  register,
};
