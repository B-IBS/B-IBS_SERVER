import updateFireBaseToken from './updateFireBaseToken';

export default {
  Query: {
  },
  Mutation: {
    ...updateFireBaseToken,
  },
};
