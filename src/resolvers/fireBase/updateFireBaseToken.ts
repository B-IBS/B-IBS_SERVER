import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

interface UpdateFireBaseTokenArg {
  token: string;
}

/*
 * mutation: updateFireBaseToken(token: String!): Boolean!
 * @param: token String! : Fire base token.
 * @return: Boolean : True if the token have been successfully updated
 *
 * Update or add the Fire base token of the connected user.
 */
const updateFireBaseToken = async (parent: any, arg: UpdateFireBaseTokenArg, context: any): Promise<boolean> => {
  if (!context.request.user) {
    throw new AuthError(401, 'This query need a connected user');
  }

  let fcUser = await prisma.fireBaseIDs({
    where: {
      user: { id: context.request.user.id },
    },
  });

  if (fcUser.length === 0) {
    fcUser = [
      await prisma.createFireBaseID({
        fcId: arg.token,
        user: {
          connect: { id: context.request.user.id },
        },
      }),
    ];
  }
  await prisma.updateFireBaseID({
    data: { fcId: arg.token },
    where: { id: fcUser[0].id },
  });
  return true;
};

export default {
  updateFireBaseToken,
};
