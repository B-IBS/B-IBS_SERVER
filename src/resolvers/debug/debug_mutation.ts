import RuntimeError = WebAssembly.RuntimeError;

const helloworld = async (_, __, ___): Promise<null> => {
    throw RuntimeError;
};

export default {
    helloworld,
};
