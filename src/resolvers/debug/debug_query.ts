import { Context } from 'graphql-yoga/dist/types';
import {prisma} from "../../generated/prisma-client";

const helloworld = async (parent: never, arg: never, context: Context): Promise<string> => {
  if (context.request.user) {
    const rest = context.request.user.id.toString().concat(' Auth: ', context.request.user.authorisation.toString());
    return rest;
  } return 'Hello world';
};

const createCrisis = async (_: never, {id}, __: Context): Promise<void> => {
  await prisma.deleteManyCrises({ user: { id: id }});

  const date = new Date('2021-01-01');
  const now = new Date();
  for (let i = 0; date.getTime() < now.getTime(); date.setDate(date.getDate() + 1), i++) {
    await prisma.createCrisis({
      date: date,
      user: {
        connect: {
          id: id
        }
      },
      intensity: Math.round((Math.cos(i / 10) * 3 + 5 + (Math.random() * 6 - 3)) / 2).toString(),
      report: ""
    })
  }
}

export default {
  helloworld,
  createCrisis
};
