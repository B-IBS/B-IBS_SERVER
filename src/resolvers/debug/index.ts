import mutHelloworld from './debug_mutation';
import queHelloworld from './debug_query';

export default {
  Query: {
    ...queHelloworld,
  },
  Mutation: {
    ...mutHelloworld,
  },
};
