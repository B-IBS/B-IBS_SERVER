import addCrisis from './addCrisis';
import getCrisis from './getCrisis';

export default {
  Query: {
    ...getCrisis,
  },
  Mutation: {
    ...addCrisis,
  },
};
