import { AuthError } from '../../error/AuthError';
import {Crisis, prisma} from '../../generated/prisma-client';

/*
 * query: getAllCrisis: [Crisis!]!
 * @return: [Crisis!]! : All User crisis
 *
 * Returns all crisis for a User
 */
const getAllCrisis = async (_: any, __: any, context: any): Promise<Crisis[]> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  return prisma.crises({
    where: { user: { id: context.request.user.id } },
  });
};

interface GetCrisisArgs {
  date: string;
}
/*
 * query: getCrisisAt(date: String!): [Crisis!]!
 * @param: date String! : Filter Date for crisis
 * @return: [Crisis!]!  : All User crisis for given Date
 *
 * Returns all crisis for a User at a given Date
 * TODO: Check if that actually works with the date ?? @paulemile
 */
const getCrisisAt = async (_: any, arg: GetCrisisArgs, context: any): Promise<Crisis[]> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  const user = await prisma.crises({
    where: {
      user: { id: context.request.user.id },
      date: arg.date.toString(),
    },
  });
  return user;
};

export default {
  getCrisisAt,
  getAllCrisis,
};
