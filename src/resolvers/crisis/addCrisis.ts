import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';
import updateGraph from "../IntelligenceArtificiel/graphManagement";

interface AddCrisisArgs {
  date: string;
  intensity: string;
  report: string;
  type: string;
}
/*
 * mutation: addCrisis(date: String! intensity: String! report: String): Boolean!
 * @param: date String!      : Date the crisis happened
 * @param: intensity String! : Intensity of the crisis
 * @param: report String!    : Description of the crisis
 * @return: Boolean!         : If Crisis was correctly added
 *
 * Create a new Crisis for a User
 */
const addCrisis = async (_: any, arg: AddCrisisArgs, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  await prisma.createCrisis({
    date: arg.date,
    user: { connect: { id: context.request.user.id } },
    intensity: arg.intensity,
    type: arg.type ? arg.type : 'not specified' ,
    report: arg.report,
  });
  await updateGraph(context.request.user.id, new Date(arg.date))
  const allCrises = await prisma.profils({ where: { user: { id: context.request.user.id } } });
  let crisis = allCrises.length > 0 ? allCrises[0] : null;
  if (!crisis) {
    crisis = await prisma.createProfil({
      day_without_crisis: 0,
      nb_medicine: 0,
      moy_crisis: 0,
      som_crisis: 0,
      nb_crisis: 0,
      nb_meal: 0,
      user: { connect: { id: context.request.user.id } },
    });
  }
  const nb = crisis.nb_crisis + 1;
  const som = crisis.som_crisis + parseInt(arg.intensity, 10);
  await prisma.updateProfil({ data: { nb_crisis: nb, som_crisis: som, moy_crisis: som / nb }, where: { id: crisis.id } });
  return true;
};

export default {
  addCrisis,
};
