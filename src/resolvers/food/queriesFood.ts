import {Food, prisma} from '../../generated/prisma-client';
import {AuthError, InvalidArgumentError} from '../../error/AuthError';
import {unsplashImageToken} from "../../config/secret_key";
import axios from 'axios';
import { compareTwoStrings } from 'string-similarity';

export interface QueryFood {
    food: Food;
    isUserFood: boolean;
    userFoodId: string;
}

interface GetFoodArgs {
    name: string;
}

/*
 * query: getFood(name: String!): QueryFood!
 * @param: name String! : Food name.
 * @return: QueryFood! : Queried Food.
 *
 * Return the detail of a specify food.
 */
const getFood = async (_: any, arg: GetFoodArgs, context: any): Promise<QueryFood> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

    const userFoods: QueryFood[] = (
        await Promise.all(
            (
                await prisma.userFoods({
                    where: {
                        food: {name: arg.name.toString()},
                        user: {id: context.request.user.id},
                    },
                })
            ).map(async (userFood) => ({
                ...userFood,
                food: await prisma.userFood({id: userFood.id}).food(),
            })),
        )
    ).map((userFood) => ({
        food: {
            ...userFood.food,
            level: userFood.level,
        },
        isUserFood: true,
        userFoodId: userFood.id,
    }));

    if (userFoods.length) return userFoods[0];

    const food: QueryFood = {
        food: await prisma.food({name: arg.name.toString()}),
        isUserFood: false,
        userFoodId: null,
    };

    if (!food) throw new InvalidArgumentError(400, `Unknown food: ${arg.name}`);
    return food;
};


const fetchImages = async (data): Promise<Array<QueryFood>> => {
    data = await Promise.all(data.map(async (element) => {
        try {
            if (element.food.imageUrl === "") {
                const response = await axios.get('https://api.unsplash.com/search/photos/?client_id=' + unsplashImageToken + '&query=' + element.food.name);
                if (response.data && response.data.results.length != 0 && response.data.results[0].urls.small) {
                    await prisma.updateFood({
                        where: {id: element.food.id}, data: {
                            name: element.food.name,
                            level: element.food.level,
                            caution: element.food.caution || false,
                            details: element.food.details?.toString() || '',
                            imageUrl: response.data.results[0].urls.small
                        }
                    })
                    return {
                        ...element,
                        imageUrl: response.data.results[0].urls.small,
                        food: {...element.food, imageUrl: response.data.results[0].urls.small}
                    }
                }
            }
        } catch (error) {
            console.error(error);
        }
        return element;
    }));
    return data;
}

const addStat = async (data): Promise<Array<QueryFood>> => {
    data = await Promise.all(data.map(async (element) => {
        const food = await prisma.food({id: element.food.id});
        const userFoodmap = await prisma.userFoods({where: {food: {id: element.food.id}}})
        const usernb = await prisma.users();
        const stat: { [id: string]: number } = {
            "0": 0,
            "1": 0,
            "2": 0,
        };
        userFoodmap.map((e) => {
            stat[e.level.toString()] += 1
        })
        stat[food.level.toString()] += usernb.length - userFoodmap.length
        return {...element, food: {...element.food, stat: {hightlevel: stat["2"], mediumlevel: stat["1"], lowlevel: stat["0"]}}}
    }));
    return data;
}

interface SearchFoodArgs {
    name: string;
    limit: number | null;
}

/*
 * query: searchFood(name: String! limit: Int): [Food!]!
 * @param name String! : Food name or partial name.
 * @param: limit Int : Max number of result.
 * @return: [QueryFood!]! : List of food that could correspond to what was asked.
 *
 * Return a list of food that can correspond to the name pass or the partial name passed as argument.
 */
const searchFood = async (_: any, arg: SearchFoodArgs, context: any): Promise<Array<QueryFood>> => {
    if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
    if (arg.name === '') return [];
    const userFoods: QueryFood[] = (
        await Promise.all(
            (
                await prisma.userFoods({
                    where: {
                        food: {name_contains: arg.name.toString()},
                        user: {id: context.request.user.id},
                    },
                })
            ).map(async (userFood) => ({
                ...userFood,
                food: await prisma.userFood({id: userFood.id}).food(),
            })),
        )
    ).map((userFood) => ({
        food: {
            ...userFood.food,
            level: userFood.level,
        },
        isUserFood: true,
        userFoodId: userFood.id,
    }));

    const foods: QueryFood[] = (
        await prisma.foods({
            where: {
                name_contains: arg.name.toString(),
            },
        })
    ).filter((food) => (
        userFoods.findIndex((userFood) => food.id === userFood.food.id) === -1
    )).map((food) => ({
        food,
        isUserFood: false,
        userFoodId: null,
    }));

    const allFoods = userFoods.concat(foods);
    const sortedFood = allFoods.sort((a, b) => compareTwoStrings(b.food.name, a.food.name));
    const limittedFoods = sortedFood.slice(0, arg.limit !== null ? arg.limit : sortedFood.length);
    return await addStat(await fetchImages(limittedFoods));
};

export default {
    getFood,
    searchFood,
};
