import queriesFood from './queriesFood';
import mutationsFood from './mutationsFood';
import queriesUserFood from './queriesUserFood';
import mutationsUserFood from './mutationsUserFood';

export default {
  Query: {
    ...queriesUserFood,
    ...queriesFood,
  },
  Mutation: {
    ...mutationsFood,
    ...mutationsUserFood,
  },
};
