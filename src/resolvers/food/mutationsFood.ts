import { prisma } from '../../generated/prisma-client';
import { InvalidArgumentError } from '../../error/AuthError';
import * as tmpFoodList from './tmpFoodList.json';
import { adminMiddleware } from '../../middleware/authentification';

interface AddFoodArgs {
  name: string;
  level: number;
  caution: boolean | null;
  details: string | null;
  imageUrl: string | null;
}
/*
 * mutation: addFood(name: String! level: String! caution: Boolean details: String imageUrl: String): Boolean!
 * @param: name String! : Food name.
 * @param: level String! : Foodmap default level.
 * @param: caution Boolean :
 * @param: details String :  Food details
 * @param: imageUrl String : Url to image that represent the food
 * @return: Type : True if the food is correctly added.
 *
 * Add a new food to the food data base.
 */
const addFood = async (_: any, arg: AddFoodArgs, __: any): Promise<boolean> => {
  const exist = await prisma.food({ name: arg.name.toString() });
  if (exist) throw new InvalidArgumentError(400, `Food already exists: ${arg.name}`);
  await prisma.createFood({
    name: arg.name.toString(),
    level: Number(arg.level),
    caution: arg.caution || false,
    details: arg.details?.toString() || '',
    imageUrl: arg.imageUrl?.toString() || '',
  });
  return true;
};

interface UpdateFoodArgs {
  foodId: string;
  name: string | null;
  level: number | null;
  caution: boolean | null;
  details: string | null;
  imageUrl: string | null;
}
/*
 * mutation: updateFood(foodId: ID! name: String level: String caution: Boolean details: String imageUrl: String): Boolean!
 * @param foodId ID! : Food to edit Id.
 * @param: name String : Food new name.
 * @param: level String: Food new level.
 * @param: caution Boolean : Food new caution.
 * @param: details String: Food new details.
 * @param imageUrl String : Food new url image.
 * @return: Boolean : True if the Food is correctly.
 *
 * Edit Food data.
 */
const updateFood = async (_: any, arg: UpdateFoodArgs, __: any): Promise<boolean> => {
  const exist = await prisma.food({ id: arg.foodId.toString() });

  if (!exist) throw new InvalidArgumentError(409, "Cette alliment n'est pas deja enregistrer");

  await prisma.updateFood({
    where: { id: arg.foodId.toString() },
    data: {
      name: arg.name?.toString() || exist.name,
      level: arg.level || exist.level,
      caution: arg.caution || exist.caution,
      details: arg.details?.toString() || exist.details,
      imageUrl: arg.imageUrl?.toString() || exist.imageUrl,
    },
  });
  return true;
};

// TODO Remove
// Temporary function
// Create an entry for each food in the tmp food list
const setFoodList = async (): Promise<boolean> => {
  const foodNames = tmpFoodList.map((e) => e.name);
  await Promise.all((await prisma.foods({
    where: {
      name_in: foodNames,
    },
  })).map(async (e) => prisma.deleteFood({ id: e.id })));
  await Promise.all(
    tmpFoodList.map(async (food) => {
      try {
        const url = '';
        await prisma.createFood({
          name: food.name,
          level: food.level,
          caution: food.caution || false,
          details: food.details?.toString() || '',
          imageUrl: url, // food?.imageUrl?.toString() || "" // this property doesnt exists in the json
        });
      } catch (e) {
        console.error(food.name, e);
      }
    }),
  );
  return true;
};

export default {
  addFood: adminMiddleware(addFood),
  updateFood: adminMiddleware(updateFood),
  setFoodList: adminMiddleware(setFoodList),
};
