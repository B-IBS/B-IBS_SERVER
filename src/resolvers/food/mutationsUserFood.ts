import { prisma } from '../../generated/prisma-client';
import { AuthError } from '../../error/AuthError';

const saveHistory = async (userId: string, foodId: string, level: number): Promise<void> => {
  await prisma.createHistoryUserFood({
    user: { connect: { id: userId } },
    food: { connect: { id: foodId } },
    level,
  });
};

interface AddUserFodMapArgs {
  level: number;
  foodId: string;
}
/*
 * mutation: addUserFodmap(foodId: ID! level: Int!): Boolean!
 * @param: foodId ID! : Food id to edit.
 * @param level Int!  : New food map level
 * @return: Boolean   : True if the Food is correctly created.
 *
 * Create a new user specify fodmap level on a food.
 */
const addUserFodmap = async (_, arg: AddUserFodMapArgs, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');

  const exist = await prisma.userFoods({
    where: {
      user: {
        id: context.request.user.id
      },
      food: {
        id: arg.foodId
      }
    }
  });
  if (exist.length == 0) {
    await prisma.createUserFood({
      user: {connect: {id: context.request.user.id}},
      food: {connect: {id: arg.foodId}},
      level: arg.level,
    });
  } else {
    await prisma.updateUserFood({
      where: { id: exist[0].id },
      data: { level: arg.level }
    });
  }
  await saveHistory(context.request.user.id, arg.foodId, arg.level);
  return true;
};

interface UpdateUserFoodMapArgs {
  userFoodId: string;
  level: number;
  foodId: string;
}
/*
 * mutation: updateUserFodmap(userFoodId: ID! level: Int! foodId: ID!): Boolean!
 * @param userFoodId ID!: FooduserId to edit.
 * @param level Int! : New foodmap level.
 * @param: foodId ID! : Food witch the id was link too.
 * @return: Boolean : True if the Food is correctly created.
 *
 * Edit userFodmap data and save the last state in an history.
 */
const updateUserFodmap = async (_: any, arg: UpdateUserFoodMapArgs, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  await prisma.updateUserFood({
    data: { level: arg.level },
    where: { id: arg.userFoodId },
  });
  await saveHistory(context.request.user.id, arg.foodId, arg.level);
  return true;
};

interface DeleteUserFoodMapArgs {
  userFoodId: string;
}
/*
 * mutation: deleteUserFodmap(userFoodId: ID!): Boolean!
 * @param: userFoodId ID! : Id of the UserFood.
 * @return: Boolean   : True if the Food is correctly created.
 *
 * Edit userFodmap data and save the last state in an history.
 */
const deleteUserFodmap = async (_: any, arg: DeleteUserFoodMapArgs, context: any): Promise<boolean> => {
  if (!context.request.user) throw new AuthError(401, 'This query need a connected user');
  await prisma.deleteUserFood({ id: arg.userFoodId });
  return true;
};

export default {
  addUserFodmap,
  updateUserFodmap,
  deleteUserFodmap,
};
