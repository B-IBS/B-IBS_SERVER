import debug from './debug';
import authentification from './authentification';
import appointment from './appointment';
import food from './food';
import crisis from './crisis';
import tuto from './tuto';
import medicines from './medicines';
import tokenConnection from './token_connection';
import report from './report';
import firebase from './fireBase';
import meal from './meal';
import profile from './profile';
import partnerCode from './partnerCode';
import recipe from './Recipe'
import error from './error'
import metrics from './metric'
import ai from './IntelligenceArtificiel'
import stress from './stress'
import water from './water'
import stool from './stool'

export default {
  Query: {
    ...debug.Query,
    ...authentification.Query,
    ...appointment.Query,
    ...food.Query,
    ...crisis.Query,
    ...tuto.Query,
    ...tokenConnection.Query,
    ...medicines.Query,
    ...report.Query,
    ...firebase.Query,
    ...meal.Query,
    ...profile.Query,
    ...partnerCode.Query,
    ...error.Query,
    ...metrics.Query,
    ...recipe.Query,
    ...ai.Query,
    ...stress.Query,
    ...water.Query,
    ...stool.Query
  },
  Mutation: {
    ...debug.Mutation,
    ...authentification.Mutation,
    ...appointment.Mutation,
    ...food.Mutation,
    ...crisis.Mutation,
    ...tuto.Mutation,
    ...medicines.Mutation,
    ...tokenConnection.Mutation,
    ...report.Mutation,
    ...firebase.Mutation,
    ...meal.Mutation,
    ...profile.Mutation,
    ...partnerCode.Mutation,
    ...error.Mutation,
    ...metrics.Mutation,
    ...ai.Mutation,
    ...recipe.Mutation,
    ...stress.Mutation,
    ...water.Mutation,
    ...stool.Mutation
  },
};
