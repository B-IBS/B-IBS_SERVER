import { prisma } from '../../generated/prisma-client';

/*
 * query: query/mutation prototype
 * @param: foo Type : argument name and it's type.
 * @return: Type : return type and a small description.
 *
 * Explain what this query/mutation does.
 */
interface CheckTutoArg {
  check: boolean;
  id: string;
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const checkTuto = async (parent: unknown, arg: CheckTutoArg, _: unknown): Promise<boolean> => {
  await prisma.updateUserTuto({ data: { cheek: arg.check }, where: { id: arg.id } });
  return true;
};

export default {
  checkTuto,
};
