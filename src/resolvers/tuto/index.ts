import checkTuto from './checkTutoArg';
import addTuto from './addTuto';

export default {
  Query: {

  },
  Mutation: {
    ...checkTuto,
    ...addTuto,
  },
};
