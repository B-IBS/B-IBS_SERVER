import { prisma } from '../../generated/prisma-client';

interface AddTutoArg {
  name: string;
  tuto: string;
}
/*
 * query: query/mutation prototype
 * @param: foo Type : argument name and it's type.
 * @return: Type : return type and a small description.
 *
 * Explain what this query/mutation does.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const addTuto = async (parent: unknown, arg: AddTutoArg, _: unknown): Promise<boolean> => {
  await prisma.createTuto({ ...arg });
  return true;
};

export default {
  addTuto,
};
