import MyError from './MyError';
import NoneMetricsError from './NoneMetricsError';

export class InvalidArgumentError extends MyError {
  constructor(public status: number, public message: string) {
    super(status, message, 'InvalidArgumentError');

    Object.setPrototypeOf(this, InvalidArgumentError.prototype);
  }
}
export class AuthError extends MyError {
  constructor(public status: number, public message: string) {
    super(status, message, 'AuthError');

    Object.setPrototypeOf(this, AuthError.prototype);
  }
}

export class NoneMetricsAuthError extends NoneMetricsError {
  constructor(public status: number, public message: string) {
    super(status, message, 'NoneMetricsAuthError');

    Object.setPrototypeOf(this, NoneMetricsAuthError.prototype);
  }
}
