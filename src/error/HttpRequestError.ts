import MyError from './MyError';

class HttpRequestError extends MyError {
  constructor(public status: number, public message: string) {
    super(status, message, 'HttpRequestError');

    Object.setPrototypeOf(this, HttpRequestError.prototype);
  }
}

export default HttpRequestError;
