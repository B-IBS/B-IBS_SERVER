class ClientError extends Error {
  constructor(public status: number, public message: string, public name: string, public stack: string, public code: string = '') {
    super();

    Object.setPrototypeOf(this, ClientError.prototype);
  }
}

export default ClientError;
