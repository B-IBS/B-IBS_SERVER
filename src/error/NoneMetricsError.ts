import MyError from './MyError';

class NoneMetricsError extends MyError {
  constructor(public status: number, public message: string, public name: string = 'NoneMetricsError') {
    super(status, message, name);

    Object.setPrototypeOf(this, NoneMetricsError.prototype);
  }
}

export default NoneMetricsError;
