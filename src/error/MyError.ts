class MyError extends Error {
  constructor(public status: number, public message: string, public name: string) {
    super();

    Object.setPrototypeOf(this, MyError.prototype);
  }
}

export default MyError;
