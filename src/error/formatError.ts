import { GraphQLError } from 'graphql';
import ClientError from './ClientError';

interface Error {
  status: number;
  name: string | null;
  message: string;
  code: string;
  stack?: string;
}

export default (error: GraphQLError): Error => {
  if (error.originalError instanceof ClientError) {
    if (process.env.DEV) {
      return {
        status: error.originalError.status, message: error.originalError.message, name: error.originalError.name, code: error.originalError.code, stack: error.originalError.stack,
      };
    } return {
      status: error.originalError.status, message: error.originalError.message, name: error.originalError.name, code: error.originalError.code,
    };
  }
  console.error(error);
  return {
    status: 500,
    name: "Erreur inconnus",
    message: "Une erreur s'est produite",
    code: '',
  };
};
