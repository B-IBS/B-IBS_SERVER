import { CronJob } from 'cron';
import { prisma } from '../generated/prisma-client';

export default new CronJob('59 23 * * *', async () => {
    const users = await prisma.users();
    let diff;
    users.forEach(async (user) => {
        const crisis = await prisma.crises({ where: { user: { id: user.id } } });
        let days = -1;
        if (crisis.length > 0)  {
            crisis.forEach((c) => {
                diff = Math.floor(Date.now() / 86400000 - new Date(c.date).getTime() / 86400000);
                if (days == -1)
                    days = diff
                if (days > diff)
                    days = diff
            })
            const pro = await prisma.profils({ where: { user: { id: user.id } } });
            await prisma.updateProfil({ data: { day_without_crisis: days }, where: { id: pro[0].id } });
        }
    });


});
