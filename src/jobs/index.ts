import debug from './debug';
import smallTokenRemover from './smallTokenRemove';
import notification from './Notification';
import intervalCrisis from './intervalCrisis';

export default (): void => {
  debug.start();
  intervalCrisis.start();
  smallTokenRemover.start();
  notification();
};
