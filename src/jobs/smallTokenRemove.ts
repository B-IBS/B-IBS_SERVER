import { CronJob } from 'cron';
import { prisma } from '../generated/prisma-client';

export default new CronJob('5 8 * * 0', async () => {
  await prisma.deleteManyTokenConnections({ expiration_date_lte: new Date() });
});
