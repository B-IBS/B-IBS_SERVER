import { CronJob } from 'cron';
import { prisma } from '../../generated/prisma-client';
import NotificationWrapper from '../../tools/notification/NotificationWrapper';

/*const findUserCrisis = async (): Promise<[string]> => {
  const date = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
  const query = `
  query {
    crises (where: {date_gt: "${date.toISOString().split('T')[0]}"}) {user {id}}
  }
  `;
  const data = await prisma.$graphql(query);
  return data.crises.map((item) => item.user.id).filter((value, index, self) => self.indexOf(value) === index);
};*/
// Get all user that have not summit a crises for more that 1 week

const findUserMeal = async (): Promise<[string]> => {
  const date = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
  const query = `
  query {
    meals(where: {date_gt: "${date.toISOString().split('T')[0]}"}) {user {id}}
    }
  `;
  const data = await prisma.$graphql(query);
  let firebase = await prisma.$graphql('query {fireBaseIDs {id fcId user {id}}}');
  const unique = data.meals.map((item) => item.user.id).filter((value, index, self) => self.indexOf(value) === index);
  firebase = firebase.fireBaseIDs.filter((item) => !unique.includes(item.user.id)).map((item) => item.fcId);
  return firebase;
};

const sendNotifications = async (): Promise<void> => {
  const wrapper = new NotificationWrapper();
  const message = {
    notification: {
      title: 'BIBS',
      body: 'N\'oubliez pas de remplir vos repas !',
    },
    android: {
      notification: {
        click_action: 'CLICK_ACTION',
      },
    },
    data: {
      routeTo: '/newCrisis',
    },
  };
  const user = (await findUserMeal())
    .filter((e) => e !== null)
    .filter((e, i, arr) => arr.findIndex((e2) => e2 === e) === i) as [string];
  if (user.length) {
    await wrapper.sendNotifications(user, message);
  }
};

export default new CronJob('1 * * * *', sendNotifications);
