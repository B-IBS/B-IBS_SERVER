import { prisma } from '../generated/prisma-client';
import NoneMetricsError from '../error/NoneMetricsError';
import HttpRequestError from '../error/HttpRequestError';
import MyError from '../error/MyError';
import ClientError from '../error/ClientError';

// Graphql middleware pour la gestion d'erreur.
export const errorMiddlewareGraphql = async (resolve, root, args, context, info): Promise<any> => {
  try {
    return await resolve(root, args, context, info);
  } catch (err) {
    let user = null;
    if (context.request.user) {
      user = context.request.user.id;
    }
    if (err instanceof MyError) {
      let savedError;
      if (user) {
        savedError = await prisma.createErrorLog({
          name: err.name,
          code: err.status,
          args,
          error: err.toString(),
          user: { connect: { id: user } },
          requestName: info.fieldName.toString(),
        });
      } else {
        savedError = await prisma.createErrorLog({
          name: err.name,
          code: err.status,
          args,
          error: err.toString(),
          requestName: info.fieldName.toString(),
        });
      }
      throw new ClientError(err.status, err.message, err.name, `${err.message}\n${err.stack}`, savedError.id);
    } else if (err instanceof NoneMetricsError) throw new ClientError(err.status, err.message, err.name, `${err.message}\n${err.stack}`);
    else {
      let savedError;
      if (user) {
        savedError = await prisma.createErrorLog({
          name: err.name,
          args,
          code: 500,
          error: err.toString(),
          user: { connect: { id: user } },
          requestName: info.fieldName.toString(),
        });
      } else {
        savedError = await prisma.createErrorLog({
          name: err.name,
          args,
          code: 500,
          error: err.toString(),
          requestName: info.fieldName.toString(),
        });
      }
      throw new ClientError(500, 'Erreur 500', err.name, `${err.message}\n${err.toString()}`, savedError.id);
    }
  }
};

// Express middleware pour la gestion d'erreur.
export const errorMiddlewareHttp = async (err, req, res, _): Promise<any> => {
  let user = null;
  if (req.user) user = req.user.id;
  let savedError;
  if (err instanceof HttpRequestError) {
    if (user) {
      savedError = await prisma.createErrorLog({
        name: err.name,
        code: err.status,
        args: {},
        error: err.toString(),
        user: { connect: { id: user } },
        requestName: req.originalUrl,
      });
    } else {
      savedError = await prisma.createErrorLog({
        name: err.name,
        code: err.status,
        args: {},
        error: err.toString(),
        requestName: req.originalUrl,
      });
    }
    res.status(err.status).send(err.status, err.message, err.name, savedError.id);
  } else if (err instanceof NoneMetricsError) res.status(err.status).send(err.status).send(err.status, err.message, err.name, savedError.id);
  else {
    if (user) {
      savedError = await prisma.createErrorLog({
        name: err.name,
        args: {},
        code: 500,
        error: err.toString(),
        user: { connect: { id: user } },
        requestName: req.originalUrl,
      });
    } else {
      savedError = await prisma.createErrorLog({
        name: err.name,
        code: 500,
        args: {},
        error: err.toString(),
        requestName: req.originalUrl,
      });
    }
    res.status(500).send(err.status, 'Erreur 500', err.name, savedError.id);
  }
};
