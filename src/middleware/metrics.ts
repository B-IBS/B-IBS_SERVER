import { prisma } from '../generated/prisma-client';

// Graphql middleware la metrics.
export default async (resolve, root, args, context, info): Promise<any> => {
  let user = null;
  if (context.request.user) {
    user = context.request.user.id;
  }
  if (user) {
    await prisma.createMetricsRequest({
      requestName: info.fieldName.toString(),
      user: { connect: { id: user } },
      authorisation: context.request.user.authorisation,
    });
  } else {
    await prisma.createMetricsRequest({
      requestName: info.fieldName.toString(),
    });
  }
  return resolve(root, args, context, info);
};
