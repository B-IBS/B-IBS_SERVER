import { errorMiddlewareGraphql } from './error';
import metricsMiddlewareGraphQl from './metrics';
import { authenticationMiddlewareGraphQl } from './authentification';

const middlewareReslover = [authenticationMiddlewareGraphQl, errorMiddlewareGraphql, metricsMiddlewareGraphQl];

export default middlewareReslover;
