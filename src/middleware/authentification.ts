import * as jwt from 'jsonwebtoken';
import { jwtKey } from '../config/secret_key';
import { prisma } from '../generated/prisma-client';
import expiresIn from '../config/Jwt';
import ClientError from '../error/ClientError';
import MyError from '../error/MyError';
import { AuthError } from '../error/AuthError';

const getAuthToken = (authorizationHeader: string): string => {
  const [type, token] = authorizationHeader?.split(' ') ?? []; // handle undefined authorization header

  if (type !== 'Bearer' || !token) {
    throw jwt.JsonWebTokenError;
  }
  return token;
};

export const adminMiddleware = (fn) => {
  return async (parent, arg, ctx): Promise<any> => {
    if (!ctx.request.user) throw new AuthError(401, 'This query need a connected user');
    if (!ctx.request.user.isAdmin) throw new AuthError(401, 'This query needs an admin user');
    return await fn(parent, arg, ctx);
  }
}

export const authenticationMiddlewareGraphQl = async (resolve, root, args, context, info): Promise<any> => {
  if (!context.request.headers.authorization) {
    context.request.auth_error = { status: false, mess: 'Missing token', code: 401 };
    return resolve(root, args, context, info);
  }
  try {
    const payload = await jwt.verify(getAuthToken(context.request.headers.authorization), jwtKey);
    const exist = await prisma.user({ id: payload.id });
    if (exist && Date.now() < payload.exp * 1000) {
      context.request.user = { id: payload.id, authorisation: payload.authorisation, isAdmin: exist.isAdmin || false };
      const token = jwt.sign({ id: payload.id, authorisation: payload.authorisation }, jwtKey, {
        algorithm: 'HS256',
        expiresIn,
      });
      // Remove/Change cookie based response ?
      context.response.cookie('token', token, { maxAge: expiresIn * 1000 });
    } else context.request.auth_error = { status: false, mess: 'User not found', code: 401 };
    return await resolve(root, args, context, info);
  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      context.request.auth_error.auth_error = { status: false, mess: 'JWT is unauthorized', code: 401 };
    } else if (!(e instanceof MyError)) {
      throw e;
    } else {
      const savedError = await prisma.createErrorLog({
        name: e.name,
        args,
        code: 500,
        error: e.toString(),
        requestName: 'Auth Error',
      });
      throw new ClientError(500, 'Erreur 500', e.name, savedError.id, `${e.message}\n${e.stack}`);
    }
    return resolve(root, args, context, info);
  }
};

export const authenticationMiddlewareHttp = async (req, res, next): Promise<any> => {
  if (!req.headers.authorization) {
    req.auth_error = { status: false, mess: 'Missing token', code: 401 };
    next();
    return;
  }
  try {
    const payload = await jwt.verify(getAuthToken(req.headers.authorization), jwtKey);
    const exist = await prisma.user({ id: payload.id });
    if (exist && Date.now() < payload.exp * 1000) {
      req.user = { id: payload.id, authorisation: payload.authorisation };
      const token = jwt.sign({ id: payload.id, authorisation: payload.authorisation }, jwtKey, {
        algorithm: 'HS256',
        expiresIn,
      });
      // Remove/Change cookie based response ?
      res.cookie('token', token, { maxAge: expiresIn * 1000 });
    } else {
      req.auth_error = { status: false, mess: 'User not found', code: 401 };
    }
    next();
  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      req.auth_error = { status: false, mess: 'JWT is unauthorized', code: 401 };
    } else {
      req.auth_error = { status: false, mess: 'Bad request', code: 400 };
    }
    next();
  }
};
