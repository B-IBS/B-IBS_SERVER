import { GraphQLServer } from 'graphql-yoga';
import * as cors from 'cors';
import resolvers from '../resolvers';
import { prisma } from '../generated/prisma-client';
import { authenticationMiddlewareHttp } from '../middleware/authentification';
import { errorMiddlewareHttp } from '../middleware/error';
import middlewareReslover from '../middleware';
import setRoutes from '../routes/index';
import cookieParser = require('cookie-parser');

const SCHEMA_PATH = (process.env.SCHEMA_PATH ? process.env.SCHEMA_PATH : './src/schema.graphql');
const server = new GraphQLServer({
  middlewares: middlewareReslover.map((e) => ({ Query: e, Mutation: e })),
  typeDefs: SCHEMA_PATH,
  resolvers,
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  context: (request) => (
    { ...request, prisma }),
});

server.use(cors({
  origin: ["http://localhost:3000", "bibs-web.vercel.app"],
  credentials: true,
}));
server.express.use(cors({
  origin: ["http://localhost:3000", "bibs-web.vercel.app"],
  credentials: true,
  exposedHeaders: ['Content-Disposition']
}));
server.use(cookieParser());
server.express.use(cookieParser());
server.express.use(authenticationMiddlewareHttp);
server.use(errorMiddlewareHttp);

setRoutes(server.express);

export default server;
