const cron = require("node-cron");
const { exec } = require('child_process');
const fs = require("fs");
const findRemoveSync = require('find-remove');

const DATABASE_BACKUP_DIR = "./backup_database/"
const PRISMA_DIR = "../prisma/"
const LOG_DIR = './logs/'
const MAX_BACKUP_DAY = 7

const fatal_error = (err) => {
    if (err) {
        console.log(err)
        process.exit(1)
    }
}

if (!fs.existsSync(DATABASE_BACKUP_DIR)) {
    fs.mkdirSync(DATABASE_BACKUP_DIR)
}

if (!fs.existsSync(LOG_DIR)) {
    fs.mkdirSync(LOG_DIR)
}

const debug = cron.schedule("1 * * * * *", function () {
    const date = new Date().toString().split(' ').join('_')

    exec("cd " + PRISMA_DIR + " && prisma export -p tmp.zip", (error, stdout, stderr) => {
        if (error) {
            console.log("Extract exit with code: " + error.code)
            console.log("Check: " + LOG_DIR + 'backup_err.txt to get more details')
            console.log("Date: " + date + "\n\n")
            fs.appendFile(LOG_DIR + 'backup_err.txt', 'Date: ' + date + '\nError: ' + error + '\n\n\n', fatal_error)
            return;
        }
        if (stdout !== '')
            fs.appendFile(LOG_DIR + 'backup_log.txt', "Date: " + date + "\n" + "STDOUT: " + stdout + '\n\n', fatal_error);
        if (stderr !== '')
            fs.appendFile(LOG_DIR + 'backup_stderr.txt', "STDERR: " + stderr + '\n\n', fatal_error);
        if (!error) {
            fs.rename(PRISMA_DIR + 'tmp.zip', DATABASE_BACKUP_DIR + 'databasebackup_' + date + '.zip', (err) => {
                if (err)
                    fs.appendFile(LOG_DIR + 'backup_err.txt', err.toString() + '\n\n', fatal_error)
                else {
                    const file = fs.readdirSync(DATABASE_BACKUP_DIR)
                    console.log(DATABASE_BACKUP_DIR + 'databasebackup_' + date + '.zip saved')
                    if (file.length > MAX_BACKUP_DAY) {
                        const deleted = findRemoveSync(DATABASE_BACKUP_DIR, {age: {seconds: 60}, limit: file.length - MAX_BACKUP_DAY, extensions: ['.zip']});
                        console.log(deleted)
                    }
                }

            });
        }
    });
})