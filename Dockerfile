FROM node:15-alpine

ENV PORT=4000
ENV PRISMA_NETWORK=http://prisma:4466
ENV SCHEMA_PATH="/app/src/schema.graphql"

EXPOSE 4000
RUN npm install -g prisma1
WORKDIR /app
COPY package.json /app
RUN npm install --legacy-peer-deps
COPY . /app
RUN cd prisma && prisma1 generate
RUN npm run-script build
CMD ["/app/entrypoint.sh"]