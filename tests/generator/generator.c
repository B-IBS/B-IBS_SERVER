#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
    char *name = malloc(20);
    char *path = malloc(20);
    char *method = malloc(20);
    char *Expected = malloc(200);

    if (name == NULL || path == NULL || method == NULL || Expected == NULL)
        return(84);
    printf("file name : \n");
    scanf("%s", name);
    printf("path /...: \n");
    scanf("%s", path);
    printf("method post or get : \n");
    scanf("%s", method);
    printf("Expected value :\n");
    scanf("%s", Expected);
    FILE* fichier = fopen(name, "w+");
    fprintf(fichier, "meta:\n"
                     "  name: %s\n"
                     "configuration:\n"
                     "  host: back-end\n"
                     "  scheme: http\n"
                     "  port: 4000\n", name);
    fprintf(fichier, "specs:\n"
                     "  - name: %s\n"
                     "    request:\n"
                     "      path: %s\n"
                     "      method: %s\n"
                     "    response:\n"
                     "      status_code: 200\n"
                     "      headers:\n"
                     "        - name: content-type\n"
                     "          value: !!js/regexp application/json\n"
                     "      json_data:\n"
                     "        - path: $.message\n"
                     "          value: %s", name, path, method, Expected);
    fclose(fichier);
    free(name);
    free(path);
    free(method);
    free(Expected);
}