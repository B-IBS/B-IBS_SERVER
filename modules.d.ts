declare namespace NodeJS {
  export interface ProcessEnv {
    JWT_KEY: string;
    PRISMA_NETWORK: string;
    DEV: string;
    GOOGLE_APPLICATION_CREDENTIALS: string;
    FCM_KEY: string;
    EDAMAM_ID: string;
    EDAMAM_KEY: string;
  }
}
